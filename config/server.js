'use strict'

// koa an koa moddlewear
const Koa = require('koa')
const bodyParser = require('koa-bodyparser')
const logger = require('koa-logger')
const convert = require('koa-convert')
const cors = require('kcors')

// Custom middlewear
const errorHandler = require('../api/middlewear/errorHandler')
const ensureJSONAPIHeader = require('../api/middlewear/ensureJSONAPI')
const applyModuleRoutes = require('../api/routes/')
const config = require('./index')

const app = new Koa()

// General Purpose Middlewear
app.use(convert(cors({credentials: true})))
app.use(bodyParser())
if (config.enableLogging) {
	app.use(convert(logger()))
}
app.use(errorHandler())
app.use(ensureJSONAPIHeader())

// Api Routes
applyModuleRoutes(app)
module.exports = app
