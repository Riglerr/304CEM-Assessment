'use strict'
let connection = null

module.exports = function getConnection(mongoose) {
	if (connection === null) {
		connection = mongoose.createConnection()
	}
	return connection
}
