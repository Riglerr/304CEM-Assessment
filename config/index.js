'use strict'

const env = process.env.NODE_ENV || 'development'
const config = require(`./env/${env}.js`)

config.saltRounds = 10
config.jwtSecret = ''
module.exports = config
