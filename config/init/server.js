'use strict'

const config = require('../index')
const server = require('../server')

/**
 * Initialises a new Koa Server with preset Routes and Middlewear
 *
 * @returns {Promise} - Resolve: Koa Server, Reject: {Error}
 */
module.exports = () =>
	new Promise((resolve, reject) => {

		server.listen(config.port, (err) => {
			if (err) {
				return reject(err)
			}
			console.log(`listening on ${config.port}`)
			console.log(`process: ${process.pid}`)
			resolve(server)
		})
	})
