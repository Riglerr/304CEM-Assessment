'use strict'


// DB
const mongoose = require('mongoose')
const co = require('co')

mongoose.Promise = global.Promise
const autoIncrement = require('mongoose-auto-increment')
const config = require('../index')
const getDbConnection = require('../getDbConnection')

/**
 * @description Initializes and opens a connection to a pre-configured Mongo Database
 * @returns {Promise<Object>} Db Connection
 */
module.exports = co.wrap(function* () {
	const connection = getDbConnection(mongoose)

	if(connection._hasOpened) return connection
	autoIncrement.initialize(connection)
	return yield openConnection(connection)
})

const openConnection = connection => co(function* () {
	yield connection.openUri(config.dbUri)
	console.log('Mongo Connection Established')
})


// /**
//  * @description Applies default event handlers (connect, dissconect, interrupt)
//  * to a Mongoose connection
//  * @param {MongooseConnection} connection - The Mongoose Connection
//  * @returns {undefined} Undefined
//  */
// function applyEventHandlers(connection) {
// 	// If the connection throws an error
// 	connection.on('error', (err) => {
// 		console.log(`Mongoose default connection error: ${err}`)
// 	})

// 	// When the connection is disconnected
// 	connection.on('disconnected', () => {
// 		console.log('Mongoose default connection disconnected')
// 	})

// 	// When process stopped
// 	process.on('SIGINT', () => {
// 		connection.close(function() {
// 			console.log('Mongoose default connection disconnected through app termination')
// 			process.exit(exit_code)
// 		})
// 	})
// }

