'use strict'
const { expect } = require('chai')
const bcrypt = require('bcrypt')
const co = require('co')
const User = require('../api/models/user.model.js')

const initDb = require('../config/init/db')
// Custom Errors
const InvalidCredentialsError = require('custom-errors/auth/InvalidCredentialsError')
const { before, after } = require('mocha')

describe('user tests', () => {
	before( () => co( function* () {
		yield initDb()
	}))
	describe('User Utilities', function() {
		describe('hashPassword', function() {
			const password = 'ThisisatestPassword'

			it('should return a correct hash', function(done) {
				co(function* () {
					try {
						const hash = yield User.hashPassword(password)
						const match = bcrypt.compareSync(password, hash)

						expect(match).to.equal(true)
						done()
					} catch (err) {
						done(err)
					}
				})
			})
			it('should throw InvalidCredentialsError if password parameter is null', function(done) {
				co(function* () {
					try {
						yield User.hashPassword(null)
						done('This should have thrown an InvalidArgumentError')
					} catch (err) {
						expect(err).to.be.an.instanceof(InvalidCredentialsError)
						done()
					}
				})
			})
			it('should throw InvalidCredentialsError if password parameter is not a string', function(done) {
				co(function* () {
					try {
						const magicNumber = 123

						yield User.hashPassword(magicNumber)
						done('This should have thrown an InvalidArgumentError')
					} catch (err) {
						expect(err).to.be.an.instanceof(InvalidCredentialsError)
						done()
					}
				})
			})
		})
	})
	describe('User Model Tests', function() {
		describe('Password Validation', function() {
			const invalidPassword = 'IncorrectPassword'
			const validPassword = 'teSt123!'
			const testUser = new User({
				username: 'test',
				password: validPassword
			})

			before(() => co(function* (){
				yield User.remove()
				yield testUser.save()
			}))

			after(() => co(function* () {
				yield User.remove()
			}))

			it('should return true if the passwords match successfully', function(done) {
				co(function* () {
					const result = yield testUser.matchPasswordWithHash(validPassword)

					expect(result).to.equal(true)
				}).then(done, done)
			})
			it('should return false if the password & hash do not match', function(done) {
				co(function* () {
					const result = yield testUser.matchPasswordWithHash(invalidPassword)

					expect(result).to.equal(false)
				}).then(done, done)
			})
			it('should return false if a password is not provided', () => co(function* () {
				const result = yield testUser.matchPasswordWithHash()

				expect(result).to.equal(false)
			}))
			it('should not rehash password if it has not been changed', function(done) {
				co(function* () {
					const currentHash = testUser.password

					testUser.username = 'Updated'
					yield testUser.save()
					const userAfterSave = yield User.findByUsername('Updated')

					expect(userAfterSave.password).to.equal(currentHash)
				}).then(done)
			})
			it('should fail to a save a user if password is empty or null', function(done) {
				co(function* () {
					try {
						testUser.password = ''
						yield testUser.save()
						done('Failure: An error was not thrown when saving an invalid password')
					} catch (err) {
						expect(err).to.not.equal(null)
						done()
					}
				})
			})
		})
		describe('findByUsername', function() {
			const testUser = new User({ username: 'testUser', password: 'testPassword' })

			before(function(done) {
				co(function* () {
					try {
						yield User.remove()
						yield testUser.save()
						done()
					} catch (err) {
						done(err)
					}
				})
			})
			after(function(done) {
				co(function *() {
					yield User.remove()
					done()
				})
			})
			it('should successfully return a user by username', function(done) {
				co(function* () {
					try {
						const user = yield User.findByUsername('testUser')

						expect(user.username).to.equal('testUser')
						done()
					} catch(err) {
						done(err)
					}
				})
			})
			it('should throw a UserNotFoundError if a user cannot be found', function(done) {
				co(function* () {
					try {
						yield User.findByUsername('qwertyuiopasdfgjkl;')
						done('This should have thrown an esception')
					} catch(err) {
						expect(err.name).to.equal('NotFoundError')
						done()
					}
				})
			})
			it('should throw InvalidArgumentError if username parameter is null', function(done) {
				co(function*(){
					try {
						yield User.findByUsername(null)
						done('This should have thrown an exception')
					} catch(err) {
						expect(err.name).to.equal('InvalidArgumentError')
						done()
					}
				})
			})
			it('should thrown InvalidArgumentError if username parameter has an incorrect type', function(done) {
				co(function*(){
					try {
						yield User.findByUsername([])
						done('This should have thrown an exception')
					} catch(err) {
						expect(err.name).to.equal('InvalidArgumentError')
						done()
					}
				})
			})
		})
	})
})


