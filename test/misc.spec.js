'use strict'

const { expect } = require('chai')
const getIdFromRequest = require('../lib/request/getIdFromRequest')
const isInteger = require('../lib/request/isInteger')
const getCredentialsFromRequest = require('../lib/request/getCredentialsFromRequest')
const validateLocationQuery = require('../lib/request/validateLocationQuery')
const getAuthorizationHeaderFromRequest = require('../lib/request/getAuthorizationHeaderFromRequest')
const { InvalidArgumentError, InvalidLocationQueryError } = require('custom-errors')
/* eslint-disable global-require */
const getServer = () => {
	require('../config/server')
}
/* eslint-enable global-require */

describe('Request Utilities', function() {
	describe('isInteger', function() {
		it('should return false for non-number types.', function() {
			const one = 1
			const stringResult = isInteger('1')
			const objectResult = isInteger({})
			const arrayResult = isInteger([one])

			expect(stringResult).to.equal(false)
			expect(objectResult).to.equal(false)
			expect(arrayResult).to.equal(false)
		})
		it('should return false for floating point numbers', function() {
			const float = 1.1
			const result = isInteger(float)

			expect(result).to.equal(false)
		})
		it('should return true for all integers', function() {
			const result = isInteger(1)

			expect(result).to.equal(true)
		})
	})

	describe('getIdFromRequestParams', function() {
		it('should fail when no params parameter given', function() {
			try {
				getIdFromRequest()
				expect(true).to.equal(false)
			} catch(err) {
				expect(err).to.be.an('object')
			}
		})

		it('should fail if the params obejct does not contain an id property', function() {
			const fn = getIdFromRequest.bind(null, {test: 'test'} )

			expect(fn).to.throw(Error)
		})

		it('should fail when the id parameter is not a string or a number', function() {
			const fn = getIdFromRequest.bind(null, {test: 'test'} )

			expect(fn).to.throw(Error)
		})
		it('should fail when it cannot parse a string id to an integer', function() {
			const fn = getIdFromRequest.bind(null, {test: 'test'} )

			expect(fn).to.throw(Error)
		})
		it('should fail when id is a floating-point number', function() {
			const fn = getIdFromRequest.bind(null, {test: 'test'} )

			expect(fn).to.throw(Error)
		})
		it('should return an integer if successful', function() {
			const params = {
				id: 12
			}

			try {
				const id = getIdFromRequest(params)

				expect(id).to.equal(params.id)
			} catch(err) {
				expect(err).to.equal(false)
			}
		})
	})

	describe('getCredentialsFromRequest', function() {
		const ctx = {
			req: {
				body: {
					username: 'Test',
					password: 'TestPassword'
				}
			}
		}

		it('should return object with username, password', function() {
			const credentials = getCredentialsFromRequest(ctx.req)

			expect(credentials).to.not.equal(null)
			expect(credentials.username).to.equal(ctx.req.body.username)
			expect(credentials.password).to.equal(ctx.req.body.password)
		})
		it('should return null if the request does not have a body', function() {
			const credentials = getCredentialsFromRequest({req: {}})

			expect(credentials).to.equal(null)
		})
	})

	describe('validateLocationQuery', function() {
		describe('validate input parameter', function() {
			it('should throw an InvalidArgumentError if a request object is null', function() {
				try {
					validateLocationQuery(null)
					throw new Error('validateLocationQuery did not throw an exception')
				} catch(err) {
					expect(err).to.be.an.instanceof(InvalidArgumentError)
				}
			})
			it('should throw an InvalidArgumentError if the request parameter is not an object', function() {
				try {
					validateLocationQuery(['123'])
					throw new Error('validateLocationQuery did not throw an exception')
				} catch(err) {
					expect(err).to.be.an.instanceof(InvalidArgumentError)
				}
			})
			it('should throw an InvalidArgumentError if the request parameter does not contain the query object', function() {
				try {
					validateLocationQuery({ abc: '123'})
					throw new Error('validateLocationQuery did not throw an exception')
				} catch(err) {
					expect(err).to.be.an.instanceof(InvalidArgumentError)
				}
			})
			it('should throw an InvalidArgumentError if the request.query is not an object', function() {
				try {
					validateLocationQuery({query: ['123']})
					throw new Error('validateLocationQuery did not throw an exception')
				} catch(err) {
					expect(err).to.be.an.instanceof(InvalidArgumentError)
				}
			})
		})
		describe('validating the query object', function() {
			it('should throw an exception if the latitude, or longitude properties are null', function() {
				try {
					const data = {
						query: {
							latitude: null,
							longitude: '1.234'
						}
					}

					validateLocationQuery(data)
					throw new Error('validateLocationQuery did not throw an exception')
				} catch(err) {
					expect(err).to.be.an.instanceof(InvalidLocationQueryError)
				}
				try {
					const data = {
						query: {
							latitude: '1.123545',
							longitude: null
						}
					}

					validateLocationQuery(data)
					throw new Error('validateLocationQuery did not throw an exception')
				} catch(err) {
					expect(err).to.be.an.instanceof(InvalidLocationQueryError)
				}
			})
			it('should throw an exception if the latitude or longitude properties are not floats/integers', function() {
				try {
					const data = {
						query: {
							latitude: 'abcdefg',
							longitude: '1.234'
						}
					}

					validateLocationQuery(data)
					throw new Error('validateLocationQuery did not throw an exception')
				} catch(err) {
					expect(err).to.be.an.instanceof(InvalidLocationQueryError)
				}
				try {
					const data = {
						query: {
							latitude: 1.234,
							longitude: 'asdjhkfa'
						}
					}

					validateLocationQuery(data)
					throw new Error('validateLocationQuery did not throw an exception')
				} catch(err) {
					expect(err).to.be.an.instanceof(InvalidLocationQueryError)
				}
			})
			it('should thrown an expection if the keyword query exists, but is not a string', function() {
				try{
					const data = {
						query: {
							latitude: '123.1234',
							longitude: '123.1234',
							keyword: ['fhjkdsjkhdfshjk']
						}
					}

					validateLocationQuery(data)
				} catch(err) {
					expect(err).to.be.an.instanceOf(InvalidLocationQueryError)
				}
			})
		})
		describe('return values', function() {
			it('should return an object containing the latitude and longitude properties', function() {
				const data = {
					query: {
						latitude: '1.2342534',
						longitude: '-3.28374286'
					}
				}
				const query = validateLocationQuery(data)

				expect(query).to.be.an('object')
				expect(query.latitude).to.equal(data.query.latitude)
				expect(query.longitude).to.equal(data.query.longitude)
			})
			it('should return an object with the latitude, longitude and keyword properties', function() {
				const data = {
					query: {
						latitude: '1.2342534',
						longitude: '-3.28374286',
						keyword: 'Apples'
					}
				}
				const query = validateLocationQuery(data)

				expect(query).to.be.an('object')
				expect(query.latitude).to.equal(data.query.latitude)
				expect(query.longitude).to.equal(data.query.longitude)
				expect(query.keyword).to.equal(data.query.keyword)
			})
		})
	})

	describe('getAuthorizationHeaderFromRequest', function() {
		it('should return null if ctx parameter is null', function() {
			const result = getAuthorizationHeaderFromRequest(null)

			expect(result).to.be.null
		})
	})

})

describe('logger environment switch', () => {
	beforeEach(() => {
		delete require.cache[require.resolve('../config/env/production')]
		delete require.cache[require.resolve('../config/env/development')]
		delete require.cache[require.resolve('../config')]
		delete require.cache[require.resolve('../config/server')]
	})
	it('should activate logger for production' ,() => {
		process.env.NODE_ENV = 'production'
		getServer()
	})
	it('should disable logger for development', () => {
		process.env.NODE_ENV = 'development'
		getServer()
	})
})
