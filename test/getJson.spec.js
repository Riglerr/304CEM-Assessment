'use strict'

// Modules
const co = require('co')
const chai = require('chai')

chai.use(require('chai-datetime'))
const expect = chai.expect
const getJson = require('get-json')

describe('getJson tests', function() {
	it('should perform a http request and return a json object', function(done) {
		const options = {
			host: 'jsonplaceholder.typicode.com',
			path: '/posts/1',
			port: 443
		}

		co(function* () {
			try {
				const data = yield getJson(options)

				if (!data) {
					done('No data')
				}
				expect(data).to.be.an('object')
				done()
			} catch(err) {
				done(err)
			}
		})
	})
})
