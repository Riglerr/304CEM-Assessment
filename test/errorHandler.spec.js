'use strict'

const {expect} = require('chai')
const co = require('co')
const { INTERNAL_SERVER_ERROR, UNAUTHORIZED, BAD_REQUEST } = require('status-codes')
const BadRequest = require('custom-errors/BadRequest')
const ExtensibleError = require('custom-errors/ExtensibleError')
const errorHandler = require('../api/middlewear/errorHandler')()
const TokenExpiredError = require('jsonwebtoken/lib/TokenExpiredError')
const JsonWebTokenError = require('jsonwebtoken/lib/JsonWebTokenError')

describe('Error Handler middlewear', () => {
	it('should use JsonWebTokenError values for jwt errors', () => {
		const ctx = {}
		const testMessage = 'A test Message'

		return co(function* () {
			yield errorHandler(ctx, () => {
				throw new JsonWebTokenError(testMessage)
			})
			expect(ctx.status).to.equal(UNAUTHORIZED.code)
			expect(ctx.body).to.eql({
				errors: [
					{
						error: 'JsonWebTokenError',
						message: testMessage
					}
				]
			})
		})
	})
	it('should add expiredAt property to error for TokenExpiredError', () => {
		const ctx = {}
		const testMessage = 'A test Message'
		const expiredAt = new Date()

		return co(function* () {
			yield errorHandler(ctx, () => {
				throw new TokenExpiredError(testMessage, expiredAt)
			})
			expect(ctx.status).to.equal(UNAUTHORIZED.code)
			expect(ctx.body).to.eql({
				errors: [
					{
						error: 'TokenExpiredError',
						message: testMessage,
						expiredAt: expiredAt
					}
				]
			})
		})
	})
	it('should return error status and body if custom error is thrown', () => {
		const ctx = {}
		const testMessage = 'A test Message'

		return co(function* () {
			yield errorHandler(ctx, () => {
				throw new BadRequest(testMessage)
			})
			expect(ctx.status).to.equal(BAD_REQUEST.code)
			expect(ctx.body).to.eql({
				errors: [
					{
						error: 'BadRequest',
						message: testMessage
					}
				]
			})
		})
	})
	it('should use 500 INTERNAL_SERVER_ERROR if custom error does not have a statusCode', () => {
		const ctx = {}

		return co(function* () {
			yield errorHandler(ctx, () => {
				throw new ExtensibleError()
			})
			expect(ctx.status).to.equal(INTERNAL_SERVER_ERROR.code)
		})
	})
	it('(PRODUCTION)(UN-CAUGHT) should hide the details', () => {
		process.env.NODE_ENV = 'production'
		const ctx = {}

		return co(function* () {
			yield errorHandler(ctx, () => {
				throw new Error('Test Error')
			})
			expect(ctx.status).to.equal(INTERNAL_SERVER_ERROR.code)
			expect(ctx.body).to.eql({errors: {error: 'An Internal Error occurred, please contact a system administrator.' }})
			process.env.NODE_ENV = 'development'
		})
	})
	it('(DEVELOPMENT)(UN-CAUGHT) should show all error details', () => {
		process.env.NODE_ENV = 'development'
		const ctx = {}

		return co(function* () {
			yield errorHandler(ctx, () => {
				throw new Error('Test Error')
			})
			expect(ctx.status).to.equal(INTERNAL_SERVER_ERROR.code)
			expect(ctx.body).to.eql({errors: {error: 'Test Error' }})
		})
	})
	it('(DEVELOPMENT)(UN-CAUGHT) should use whole object if error message is not set', () => {
		process.env.NODE_ENV = 'development'
		const ctx = {}

		return co(function* () {
			yield errorHandler(ctx, () => {
				throw new Error()
			})
			expect(ctx.status).to.equal(INTERNAL_SERVER_ERROR.code)
		})
	})
})
