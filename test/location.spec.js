'use strict'

// Test Utilities
const chai = require('chai')

chai.use(require('chai-datetime'))
const expect = chai.expect
const { before, after } = require('mocha')
const co = require('co')
const { OK, BAD_REQUEST, NOT_FOUND } = require('status-codes')

// Test Configuration
const config = require('../config')
const server = require('../config/server')
const supertest = require('supertest')
const request = supertest.agent(server.listen())
const initDb = require('../config/init/db')

const {Location} = require('../api/models')

const LocationHelper = require('location-helper')
const PlacesApi = require('places-api')
const InvalidArgumentError = require('custom-errors/InvalidArgumentError')
const LocationNotFoundError = require('custom-errors/notFound/LocationNotFoundError')

describe('Location Tests', function() {
	before(function() {
		return co(function* () {
			try {
				yield initDb()
				yield Location.remove()
				console.log('balls')
			} catch(err) {
				console.log(err)
				throw err
			}
		})
	})
	after(function*() {
		try {
			yield Location.remove()
		} catch(err) {
			console.log(err)
		}
	})
	describe('LocationHelper Utility', function() {
		const locations = new LocationHelper(config.placesApiKey)

		describe('getByLatLong', function() {
			it('should retrieve an array of locations', function(done) {
				co(function* () {
					try {
						const places = yield locations.getByLatLong('52.4094106','-1.5074453')

						expect(places).to.be.an('array')
						done()
					} catch(err) {
						console.log(err)
						done(err)
					}
				})
			})
			it('should thrown InvalidArgumentError if either lat or long are null', function(done) {
				co(function* () {
					try {
						yield locations.getByLatLong(null, '-1.5074453')

						done('This should have thrown an exception')
					} catch(err) {
						expect(err).to.be.an.instanceOf(InvalidArgumentError)
						done()
					}
				})
			})
			it('should throw InvalidArgumentError if lat or long are not strings', function(done) {
				co(function* () {
					try {
						yield locations.getByLatLong('123', ['123'])

						done('This should have thrown an exception')
					} catch(err) {
						expect(err).to.be.an.instanceOf(InvalidArgumentError)
						done()
					}
				})
			})
		})
		describe('getById', function() {
			it('should return a single normalised location', function(done) {
				co(function* () {
					try {
						const places = yield locations.getById('ChIJN1t_tDeuEmsRUsoyG83frY4')

						expect(places).to.be.an('object')
						done()
					} catch(err) {
						console.log(err)
						done(err)
					}
				})
			})
			it('should throw InvalidArgumentError if id is null', function(done) {
				co(function* () {
					try {
						yield locations.getById(null)

						done('This should have thrown an exception')
					} catch(err) {
						expect(err).to.be.an.instanceOf(InvalidArgumentError)
						done()
					}
				})
			})
			it('should throw InvalidArgumentError if id is not a string', function(done) {
				co(function* () {
					try {
						const nonStringId = 123

						yield locations.getById(nonStringId)

						done('This should have thrown an exception')
					} catch(err) {
						expect(err).to.be.an.instanceOf(InvalidArgumentError)
						done()
					}
				})
			})
			it('should throw a LocationNotFoundError if a location with that Id was not found', function(done) {
				co(function* () {
					try {
						yield locations.getById('123')

						done('This should have thrown an exception')
					} catch(err) {
						expect(err).to.be.an.instanceof(LocationNotFoundError)
						done()
					}
				})
			})
		})
	})
	describe('Location Model tests', function() {
		describe('ensureLocationExists', function() {
			const testLocation = new Location({
				_id: 'ChIJBUfhKBVLd0gRD5NreJX3vCc',
				dateAdded: new Date(),
				name: 'New Spires',
				latitude: 52.4129412,
				longitude: -1.5477674
			})

			before(function() {
				return co(function* () {
					try {
						yield testLocation.save()
					} catch(err) {
						console.log(err)
					}
				})
			})
			it('should return null if a place does not exist in mongo or the places API', function() {
				return co(function* () {
					try {
						const location = yield Location.ensureLocationExists('jklhasdfkhjafsd')

						expect(location).to.be.null
					} catch(err) {
						console.log(err)
					}
				})
			})
			it('should throw an InvalidArgumentError if placeId parameter is null', function() {
				return co(function*() {
					try {
						yield Location.ensureLocationExists(null)
						throw new Error('This should have thrown an exeption')
					} catch(err) {
						expect(err).to.be.an.instanceOf(InvalidArgumentError)
					}
				})
			})
			it('should return a place if the document could be found in the Mongo Database', function() {
				return co(function*() {
					try {
						const place = yield Location.ensureLocationExists(testLocation._id)

						expect(place).to.not.be.null
						expect(place._id).to.equal(testLocation._id)
						expect(place.dateAdded).to.equalDate(testLocation.dateAdded)
						expect(place.name).to.equal(testLocation.name)
						expect(place.latitude).to.equal(testLocation.latitude)
						expect(place.longitude).to.equal(testLocation.longitude)
						expect(place).to.be.an.instanceOf(Location)
						return
					} catch(err) {
						console.log(err)
						throw err
					}
				})
			})
			it('should return a place if it couldn\'t be found in mongo but exists on the PlacesAPI', function() {
				return co(function*() {
					try {
						const place = yield Location.ensureLocationExists('ChIJL6eIYOdLd0gRaJDqWHOw0Dw')

						expect(place).to.not.be.null
						expect(place).to.be.an.instanceOf(Location)
					} catch(err) {
						console.log(err)
						throw err
					}
				})
			})
			it('should add places that exist in PlaceApi into mongo', function() {
				return co(function* () {
					try {
						const place = yield Location.ensureLocationExists('ChIJL6eIYOdLd0gRaJDqWHOw0Dw')

						expect(place).to.not.be.null
						expect(place).to.be.an.instanceOf(Location)
					} catch(err) {
						console.log(err)
						throw err
					}
				})
			})
		})
		describe('serialize', () => {
			let testLocation, singleResult, realtionShipResult
			const testEvent = { _id: '123'}

			before(() =>
				co(function* () {
					testLocation = yield Location.ensureLocationExists('ChIJN1t_tDeuEmsRUsoyG83frY4')
					singleResult = testLocation.serialize()
					realtionShipResult = testLocation.serialize(testEvent)
				})
			)
			describe('single', () => {
				it('should serialize properties into a data object with attributes', () => {
					expect(singleResult).to.have.property('data')
					expect(singleResult.data).to.eql({
						type: 'locations',
						id: testLocation._id,
						attributes: {
							name: testLocation.name,
							dateAdded: testLocation.dateAdded,
							latitude: testLocation.latitude,
							longitude: testLocation.longitude
						}
					})
				})
				it('should have top-level self link to the individual resource', () => {
					expect(singleResult).to.have.property('links')
					expect(singleResult.links).to.eql({
						self: `https://${config.host}/locations/${testLocation._id}`
					})
				})
			})
			describe('asRelationship', () => {
				it('should serialize properties into a data object with attributes', () => {
					expect(realtionShipResult).to.have.property('data')
					expect(realtionShipResult.data).to.eql({
						type: 'locations',
						id: testLocation._id,
						attributes: {
							name: testLocation.name,
							dateAdded: testLocation.dateAdded,
							latitude: testLocation.latitude,
							longitude: testLocation.longitude
						},
						links: {
							self: `https://${config.host}/locations/${testLocation._id}`
						}
					})
				})
				it('should have top-level self link to the individual resource', () => {
					expect(realtionShipResult).to.have.property('links')
					expect(realtionShipResult.links).to.eql({
						self: `https://${config.host}/events/${testEvent._id}/relationships/location`,
						related: `https://${config.host}/events/${testEvent._id}`
					})
				})
			})
		})
	})
	describe('PlaceApi Request Tests', function() {
		const api = new PlacesApi(config.placesApiKey)
		let firstResult = {}
		let filteredKeywordResult = {}

		describe('nearbySearch', function() {
			const coventryLatLong = {
				latitude: '52.4009146',
  			longitude: '-1.5083984'
			}

			it('should return an array of places', function(done) {
				co(function* () {
					try {
						const data = yield api.nearbySearch(coventryLatLong.latitude,coventryLatLong.longitude, '500')

						if (!data) {
							done('No Data')
						}
						expect(data).to.be.an('array')
						firstResult = data
						done()
					} catch (err) {
						console.log(err)
						done(err)
					}
				})
			})
			it('should throw an exception if latitude or longitude is null', function(done) {
				co(function* () {
					try {
						const data = yield api.nearbySearch('','', '500')

						if (!data) {
							done('No Data')
						}
						done(' This should have thrown an exception')
					} catch (err) {
						expect(err).to.not.equal(null)
						done()
					}
				})
			})
			it('should filter places by a given keyword', function(done) {
				co(function* () {
					try {
						const data = yield api.nearbySearch(coventryLatLong.latitude,coventryLatLong.longitude, '500', 'curry')

						if (!data) {
							done('No Data')
						}
						expect(data).to.be.an('array')
						filteredKeywordResult = data
						expect(filteredKeywordResult.length).to.be.lessThan(firstResult.length)
						done()
					} catch (err) {
						console.log(err)
						done(err)
					}
				})
			})
		})
		describe('details', function() {
			it('should return an object containing the details of a single place', function(done) {
				co(function* () {
					try {
						const data = yield api.details('ChIJN1t_tDeuEmsRUsoyG83frY4')

						if (!data) {
							done('No Data')
						}
						expect(data).to.be.an('object')
						done()
					} catch (err) {
						done(err)
					}
				})
			})
			it('should throw an error if id is null', function(done) {
				co(function* () {
					try {
						yield api.details(null)

						done('This should have thrown an exception')
					} catch (err) {
						expect(err).to.not.equal(null)
						done()
					}
				})
			})
			it('should reject if corresponding placeid does not exist', function(done) {
				co(function* () {
					try {
						yield api.details('12352')

						done('This should throw an exception')
					} catch (err) {
						expect(err).to.be.an('object')
						done()
					}
				})
			})
		})
	})
	describe('Location Acceptance tests', function() {
		describe('GET /locations?latitude=:latitude&longitude=:longitude', function() {
			it('should return 200(OK) with a results array for a valid latitude and longitude', function(done) {
				request
				.get('/locations')
				.set('Content-type', 'application/vnd.api+json')
				.query({ latitude: '52.4009146' })
  			.query({ longitude: '-1.5083984' })
				.expect(OK.code, (err, res) => {
					if (err) {
						return done(err)
					}
					expect(res.body).to.be.an('object')
					done()
				})
			})
			it('should return 400(BAD-REQUEST) if either latitude or longitude queries are not present', function(done) {
				request
					.get('/locations')
					.set('Content-type', 'application/vnd.api+json')
					.query({ latitude: '123' })
					.expect(BAD_REQUEST.code, (err, res) => {
						if (err) {
							return done(err)
						}
						expect(res.body).to.be.an('object')
						expect(res.body.errors).to.not.be.null
						done()
					})
			})
			it('should return 400(BAD-REQUEST) if either latitude or longitude are invalid values', function(done) {
				request
					.get('/locations')
					.set('Content-type', 'application/vnd.api+json')
					.query({ latitude: '[122]' })
					.query({ longitude: '456' })
					.expect(BAD_REQUEST.code, (err, res) => {
						if (err) {
							console.dir(res)
							return done(err)
						}
						expect(res.body).to.be.an('object')
						expect(res.body.errors).to.not.be.null
						done()
					})
			})
		})
		describe('GET /locations?latitude=:latitude&longitude=:longitude&keyword=:keyword', function() {
			it('should return 200(OK) for a valid keyword', function(done) {
				request
					.get('/locations')
					.query({ latitude: '52.4009146' })
  				.query({ longitude: '-1.5083984' })
					.query({ keyword: 'restaurant' })
					.set('Content-type', 'application/vnd.api+json')
					.expect(OK.code, (err, res) => {
						if (err) {
							return done(err)
						}
						expect(res.body).to.be.an('object')
						done()
					})
			})
			it('should return 400(BAD_REQUEST) if the keyword is invalid', function(done) {
				request
				.get('/locations')
				.query({ latitude: '52.4009146' })
  			.query({ longitude: '-1.5083984' })
				.set('Content-type', 'application/vnd.api+json')
				.query({ keyword: '' })
				.expect(OK.code, (err, res) => {
					if (err) {
						return done(err)
					}
					expect(res.body).to.be.an('object')
					done()
				})
			})
		})
		describe('GET /locations/:id', function() {
			it('should return 200(OK) with a single location result for a valid palceid', function(done) {
				const placeid = 'ChIJN1t_tDeuEmsRUsoyG83frY4'

				request
					.get(`/locations/${placeid}`)
					.set('Content-type', 'application/vnd.api+json')
					.expect(OK.code, (err, res) => {
						if (err) {
							return done(err)
						}
						expect(res.body).to.be.an('object')
						done()
					})
			})
			it('should return 404(NOT_FOUND) if that placeid does not exist', function(done) {
				const placeid = '1234'

				request
					.get(`/locations/${placeid}`)
					.set('Content-type', 'application/vnd.api+json')
					.expect(NOT_FOUND.code, (err, res) => {
						if (err) {
							return done(err)
						}
						expect(res.body).to.be.an('object')
						expect(res.body.errors).to.be.an('array')
						done()
					})
			})
			it('should return 400(BAD_REQUEST) if the id is null', function(done) {
				request
					.get('/locations/')
					.set('Content-type', 'application/vnd.api+json')
					.expect(BAD_REQUEST.code, (err, res) => {
						if (err) {
							return done(err)
						}
						expect(res.body).to.be.an('object')
						expect(res.body.errors).to.be.an('array')
						done()
					})
			})
		})
	})
})
