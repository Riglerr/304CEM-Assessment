'use strict'

// Modules
const co = require('co')
const jwt = require('jsonwebtoken')
const chai = require('chai')
const { before, after } = require('mocha')

chai.use(require('chai-datetime'))
const expect = chai.expect

// Test Configuration
const config = require('../config')
const server = require('../config/server')
const supertest = require('supertest')
const request = supertest.agent(server.listen())
const initDb = require('../config/init/db')

// Models
const User = require('../api/models/user.model.js')

// Custom Utilities
const { CREATED, UNPROCESSABLE_ENTITY, BAD_REQUEST, CONFLICT, OK, NOT_FOUND, UNAUTHORIZED } = require('status-codes')
const getAuthorizationHeaderFromRequest = require('../lib/request/getAuthorizationHeaderFromRequest')
const decodeAuthorizationHeader = require('../lib/auth/decodeAuthorizationHeader')
const signJwt = require('../lib/auth/signJWT')
const authenticationMiddlewear = require('../api/middlewear/authenticate')
const validateCredentials = require('../lib/auth/validateCredentials')
const validatePassword = require('../lib/auth/validatePassword')

describe('Authentication /auth/', function() {
	const testUserData = {
		username: 'test',
		password: 'teSt123!'
	}
	const testUser = new User({
		username: testUserData.username,
		password: testUserData.password
	})

	before(function(done) {
		co.wrap(function* () {
			try {
				yield initDb()
				yield User.remove()
				yield testUser.save()
				done()
			} catch (err) {
				done(err)
			}
		})()
	})

	after(function(done) {
		testUser.remove().then(() => {
			done()
		})
	})
	describe('POST auth/login', function() {
		it('should return 200 and a jwt in the response body on successful login', function(done) {
			request
				.post('/auth/login')
				.send(testUserData)
				.expect(OK.code, (err, res) => {
					if (err) {
						return done(err)
					}
					expect(res.body.token).to.be.a('string')
					done()
				})
		})
		it('should return 422 for unsuccessful login', function(done) {
			request
				.post('/auth/login')
				.send({ username: 'test', password: 'testFail' })
				.expect(UNPROCESSABLE_ENTITY.code, done)
		})
		it('should return 404 if that user does not exist', function(done) {
			request
				.post('/auth/login')
				.send({ username: '13aSDB123', password: '123' })
				.expect(NOT_FOUND.code, done)
		})
		it('should return 400 (Bad Request) if the payload is not provided', function(done) {
			request
				.post('/auth/login')
				.send()
				.expect(BAD_REQUEST.code, done)
		})
	})
	describe('POST /auth/register', function() {
		it('should return 201 created and provide a link to the login resourse', function(done) {
			const data = {
				username: 'TestUser',
				password: 'TestUserPword'
			}

			request
				.post('/auth/register')
				.send(data)
				.expect(CREATED.code, (err, res) => {
					if (err) {
						return done(err)
					}
					const responseBody = res.body

					expect(responseBody).to.eql({
						meta: {
							login: {
								method: 'POST',
								href: `https://${config.host}/auth/login`
							}
						}
					})
					done()
				})
		})
		it('should return 400 (Bad Request) if registration payload was invalid', function(done) {
			request
				.post('/auth/register')
				.send({ test: 'test123', invalid: true })
				.expect(BAD_REQUEST.code, done)
		})
		it('should return 400 (Bad Request) if registration payload was not provided', function(done) {
			request
				.post('/auth/register')
				.send({})
				.expect(BAD_REQUEST.code, done)
		})
		it('should return 409 (Conflict) if the specified username already exists', function(done) {
			request
				.post('/auth/register')
				.send({ username: 'test', password: '12345' })
				.expect(CONFLICT.code, done)
		})
	})
	describe('Route Authentication', function() {
		const testUserData = {
			username: 'test',
			password: 'teSt123!'
		}
		const testUser = new User({
			username: testUserData.username,
			password: testUserData.password
		})
		const authHeaders = {
			valid: '',
			invalid: '',
			expired: '',
			empty: null
		}

		before(function(done) {
			co(function* () {
				try {
					yield User.remove()
					yield testUser.save()
					const tempJwt = yield signJwt(testUser)
					const noUserjwt = yield signJwt({})
					const fiddy = 50

					authHeaders.valid = 'Bearer ' + tempJwt
					authHeaders.invalidType = tempJwt
					authHeaders.invalid = 'Bearer ' + tempJwt.slice(0, fiddy)
					authHeaders.expired = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoidGVzdHkxMjMiLCJpYXQiOjE0ODEyNDY2MTAsImV4cCI6MTQ4MTI0ODQxMH0.jPwS5RQfar3trg-ong81-iBBBb4TqcrUVTN4E5JQiBY'
					authHeaders.noUser = 'Bearer' + noUserjwt
					done()
				} catch (err) {
					done(err)
				}
			})
		})
		after(function(done) {
			co(function* () {
				yield User.remove()
				done()
			})
		})
		it('should Read JWT from Authentication header', function() {
			const ctx = {
				req: {
					headers: {
						authorization: authHeaders.valid
					}
				}
			}
			const extractedHeader = getAuthorizationHeaderFromRequest(ctx)
			const sev = 7

			expect(extractedHeader).to.equal(authHeaders.valid.substring(sev))
		})
		it('should fail if no Authentication header is present', function() {
			const ctx = {
				req: {
					headers: {
						authorization: authHeaders.empty
					}
				}
			}
			const extractedHeader = getAuthorizationHeaderFromRequest(ctx)

			expect(extractedHeader).to.equal(authHeaders.empty)
		})
		it('should fail if the Authentication header is not of type \'Bearer\'', function(done) {
			const ctx = {
				req: {
					headers: {
						authorization: authHeaders.invalidType
					}
				}
			}

			try {
				getAuthorizationHeaderFromRequest(ctx)
				done('This should throw an exception')
			} catch (err) {
				expect(err).to.not.equal(null)
				done()
			}
		})
		it('should successfully decode the JWT', function(done) {
			const ctx = {
				req: {
					headers: {
						authorization: authHeaders.valid
					}
				}
			}
			const extractedHeader = getAuthorizationHeaderFromRequest(ctx)

			if (!extractedHeader) {
				throw new Error('An Error Occurred when decoding the Authorization Header')
			}
			const expectedDecodedValue = jwt.verify(extractedHeader, config.jwtSecret)

			co(function* () {
				try {
					const decodedJWT = yield decodeAuthorizationHeader(extractedHeader)

					expect(decodedJWT).to.deep.equal(expectedDecodedValue)
					done()
				} catch (err) {
					throw err
				}
			})
		})
		it('should fail if the token is invalid', function(done) {
			const ctx = {
				req: {
					headers: {
						authorization: authHeaders.invalid
					}
				}
			}
			const extractedHeader = getAuthorizationHeaderFromRequest(ctx)

			if (!extractedHeader) {
				done('Header was null')
			}
			co(function* () {
				try {
					yield decodeAuthorizationHeader(extractedHeader)

					done('Should have thrown exception')
				} catch (err) {
					done()
				}
			})
		})
		it('should fail if the token has expired', function(done) {
			const ctx = {
				req: {
					headers: {
						authorization: authHeaders.expired
					}
				}
			}
			const extractedHeader = getAuthorizationHeaderFromRequest(ctx)

			if (!extractedHeader) {
				throw new Error('An Error Occurred when decoding the Authorization Header')
			}
			co(function* () {
				try {
					yield decodeAuthorizationHeader(extractedHeader)

					done(new Error('This should have thrown an exception'))
				} catch (err) {
					expect(err.name).to.equal('TokenExpiredError')
					done()
				}
			})
		})
		it('should read user data from JWT and fetch from DB', function(done) {
			const ctx = {
				req: {
					headers: {
						authorization: authHeaders.valid
					}
				}
			}
			const extractedHeader = getAuthorizationHeaderFromRequest(ctx)

			if (!extractedHeader) {
				throw new Error('An Error Occurred when decoding the Authorization Header')
			}
			co(function* () {
				try {
					const jwtData = yield decodeAuthorizationHeader(extractedHeader)

					expect(jwtData.user).to.equal(testUser.username)
					const jwtUser = yield User.findByUsername(jwtData.user)

					expect(jwtUser.username).to.equal(testUser.username)
					expect(jwtUser.password).to.equal(testUser.password)
					done()
				} catch (err) {
					done(err)
				}
			})
		})
		it('should fail if it cannot read a user from the JWT', function(done) {
			const ctx = {
				req: {
					headers: {
						authorization: authHeaders.noUser
					}
				}
			}

			co(function* () {
				try {
					yield authenticationMiddlewear(ctx, () => null)
					done('Should have thrown user not found')
				} catch (err) {
					done()
				}
			})
		})
		it('should set www-authenticate header to \'Bearer\' if Authentication fails', (done) => {
			request.post('/events/')
				.set('content-type', 'application/vnd.api+json')
				.expect(UNAUTHORIZED.code, (err, res) => {
					if (err) return done(err)
					expect(res.headers['www-authenticate']).to.equal('Bearer')
					done()
				})
		})
	})
	describe('Utilities', function() {
		describe('validateCredentials', function() {
			const creds = {
				username: 'Test',
				password: 'TestPword'
			}
			const nullCreds = {
				username: null,
				password: null
			}

			it('should return the credentials object if valid', function() {
				try {
					const result = validateCredentials(creds)

					expect(result).to.deep.equal(creds)
				} catch (err) {
					throw err
				}
			})
			it('should throw InvalidCredentialsError if username / password are empty', function(done) {
				try {
					validateCredentials(nullCreds)
					done('This should have thrown an exception')
				} catch (err) {
					expect(err.name).to.equal('InvalidCredentialsError')
					done()
				}
			})
		})
		describe('validatePassword', function() {
			const passwordtestUser = new User({ username: 'ValidatePasswordTestUser', password: 'password' })

			before(function(done) {
				co(function* () {
					try {
						yield User.remove()
						yield passwordtestUser.save()
						done()
					} catch (err) {
						done(err)
					}
				})
			})
			after(function(done) {
				co(function* () {
					try {
						yield User.remove()
						done()
					} catch (err) {
						done(err)
					}
				})
			})

			it('should return true if the password matches the uses password', function(done) {
				co(function* () {
					try {
						const result = yield validatePassword(passwordtestUser, 'password')

						expect(result).to.equal(true)
						done()
					} catch (err) {
						done(err)
					}
				})
			})
			it('should throw IncorrectPasswordError if the password does not match', function(done) {
				co(function* () {
					try {
						yield validatePassword(passwordtestUser, 'asdkfjahskdjhf')
						done(' SHould have thrown an exception, but didn\'t')
					} catch (err) {
						expect(err.name).to.equal('IncorrectPasswordError')
						done()
					}
				})
			})
			it('should throw InvalidArgumentError if user is null / undefined', function(done) {
				co(function* () {
					try {
						yield validatePassword(null, 'asdkfjahskdjhf')
						done('Should have thrown exception if user is null')
					} catch (err) {
						expect(err.name).to.equal('InvalidArgumentError')
						done()
					}
				})
			})
			it('should throw InvalidArgumentError if user is an incorrect type', function(done) {
				co(function* () {
					try {
						yield validatePassword({ abc: '123' }, 'asdkfjahskdjhf')
						done('Should have thrown exception if user iscorrect type')
					} catch (err) {
						expect(err.name).to.equal('InvalidArgumentError')
						done()
					}
				})
			})
		})
	})
})
