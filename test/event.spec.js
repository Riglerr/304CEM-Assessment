'use strict'

// Test Utilities
const chai = require('chai')
const merge = require('lodash.merge')

chai.use(require('chai-datetime'))
const expect = chai.expect
const { before, after } = require('mocha')
const co = require('co')
//const Promise = require('bluebird')

// Test Configuration
const config = require('../config')
const server = require('../config/server')
const supertest = require('supertest')
const request = supertest.agent(server.listen())
const initDb = require('../config/init/db')

// Model
const { Event, Location, User } = require('../api/models')
const EventSerializer = require('api-serializers/event')

// Utilities
const { OK, BAD_REQUEST, FORBIDDEN, NOT_FOUND, CREATED, UNAUTHORIZED, NO_CONTENT, NOT_MODIFIED } = require('status-codes')
const validateEventPayload = require('../lib/request/validateEventPayload')

// Errors
const InvalidArgumentError = require('custom-errors/InvalidArgumentError')
const InvalidEventError = require('custom-errors/event/InvalidEventError')
const EventNotFoundError =require('custom-errors/notFound/EventNotFoundError')

const AuthController = require('../api/controllers/auth.controller.js')
const startEpoch = 100000000
const endEpoch = 10000000000
const generateStartDate = () => new Date(Date.now() + startEpoch)
const generateEndDate = () => new Date(Date.now() + endEpoch)


describe('Events: ', () => {
	before(function() {
		return co(function* () {
			yield initDb()
		})
	})
	after(function() {
		return co(function* () {
			yield Event.remove()
			yield Location.remove()
		})
	})
	describe('Acceptance Tests', function() {
		describe('GET /Events', () => {
			before(function() {
				return co(function* () {
					yield Event.remove()
					yield new Event({
						title: 'A Test Event',
						description: 'Test Event, please Ignore',
						startDate: generateStartDate(),
						endDate: generateEndDate(),
						creator: 'TestUser',
						location: 'ChIJN1t_tDeuEmsRUsoyG83frY4'
					}).save()
					yield new Event({
						title: 'Nothing',
						description: 'To See here',
						startDate: generateStartDate(),
						endDate: generateEndDate(),
						creator: 'TestUser2',
						location: 'ChIJN1t_tDeuEmsRUsoyG83frY4'
					}).save()
				})
			})
			after(() => co(function* () {
				yield Event.remove()
				yield Location.remove()
			}))
			describe('success', function() {
				let response = null

				it('should return 200 OK', (done) => {
					request
						.get('/events/')
						.set('Content-type', 'application/vnd.api+json')
						.expect(OK.code, (err, res) => {
							response = res
							if(err) return done(err)
							done()
						})
				})
				it('should contain a top-level links property', () => {
					expect(response.body).to.have.property('links').which.is.an('object')
					expect(response.body.links).to.have.property('self').which.equals('https://localhost:3000/events/')
				})
				it('should contain a top-level data array', () => {
					expect(response.body).to.have.property('data')
					expect(response.body.data).to.be.an('array')
					expect(response.body.data).to.not.be.null
				})
				describe('data array', function() {

					it('should contain at least 1 object', () => {
						expect(response.body.data).to.have.length.gte(1)
					})
					describe('data item', function() {
						it('should be of type \'events\'', () => {
							const item = response.body.data[0]

							expect(item).to.be.an('object')
							expect(item).to.not.be.null
							expect(item).to.have.a.property('type').which.equals('events')
						})
						it('should have a links property and a self link', () => {
							const item = response.body.data[0]

							expect(item).to.have.property('links')
							expect(item.links).to.have.property('self')
							expect(item.links).to.not.be.null
						})
						it('should contain all attributes', () => {
							const item = response.body.data[0]

							expect(item).to.have.property('attributes').which.is.an('object')
							expect(item.attributes).to.have.property('title').which.is.not.null
							expect(item.attributes).to.have.property('description').which.is.not.null
							expect(item.attributes).to.have.property('startDate').which.is.not.null
							expect(item.attributes).to.have.property('endDate').which.is.not.null
							expect(item.attributes).to.have.property('creator').which.is.not.null
						})
						it('should have a location relationship', () => {
							const item = response.body.data[0]

							expect(item).to.have.property('relationships').which.is.an('object')
							expect(item.relationships).to.have.property('location').which.is.an('object')
							expect(item.relationships.location).to.have.property('data').which.is.an('object')
							expect(item.relationships.location.data).to.have.property('type').which.is.a('string').which.equals('locations')
							expect(item.relationships.location.data).to.have.property('id').which.is.not.null
						})
					})
				})
			})

			describe('error', () => {
				it('should return 400 BAD_REQUEST if the client does not send header Content-Type: application/vnd.api+json', (done) => {
					request
						.get('/events/')
						.expect(BAD_REQUEST.code, done)
				})
			})
		})
		describe('GET /Events?search&sort&limit', () => {
			let e1, e2
			const noOfItems = 2

			before(function() {
				return co(function* () {
					yield Event.remove()
					e1 = yield new Event({
						title: 'A Test Event',
						description: 'Test Event, please Ignore',
						startDate: generateStartDate(),
						endDate: generateEndDate(),
						creator: 'TestUser',
						location: 'ChIJN1t_tDeuEmsRUsoyG83frY4'
					}).save()
					e2 = yield new Event({
						title: 'Nothing',
						description: 'To See here',
						startDate: generateStartDate(),
						endDate: generateEndDate(),
						creator: 'TestUser2',
						location: 'ChIJN1t_tDeuEmsRUsoyG83frY4'
					}).save()
				})
			})
			after(() => co(function* () {
				yield Event.remove()
				yield Location.remove()
			}))
			it('should return a set of items filtered by the search query', (done) => {
				request
						.get('/events/')
						.set('Content-type', 'application/vnd.api+json')
						.query({ search: 'Nothing' })
						.expect(OK.code, (err, res) => {
							if(err) return done(err)
							expect(res.body.data).to.be.an('array').with.length(1)
							expect(res.body.data[0].id).to.equal(e2._id.toString())
							done()
						})
			})
			it('should sort all items by the given field (ASCENDING)', (done) => {


				request
						.get('/events/')
						.set('Content-type', 'application/vnd.api+json')
						.query({ sort: 'title' })
						.expect(OK.code, (err, res) => {
							if(err) return done(err)
							expect(res.body.data).to.be.an('array').with.length(noOfItems)
							expect(res.body.data[0].id).to.equal(e1._id.toString())
							expect(res.body.data[1].id).to.equal(e2._id.toString())
							done()
						})
			})
			it('should sort all items by the given field (DESCENDING)', (done) => {
				request
						.get('/events/')
						.set('Content-type', 'application/vnd.api+json')
						.query({ search: 'Nothing Test' })
						.query({ sort: '-title' })
						.expect(OK.code, (err, res) => {
							if(err) return done(err)
							expect(res.body.data).to.be.an('array').with.length(noOfItems)
							expect(res.body.data[0].id).to.equal(e2._id.toString())
							expect(res.body.data[1].id).to.equal(e1._id.toString())
							done()
						})
			})
			it('should return an amount of events equal to or less than the limit value', (done) => {
				request
						.get('/events/')
						.set('Content-type', 'application/vnd.api+json')
						.query({ limit: '1' })
						.expect(OK.code, (err, res) => {
							if(err) return done(err)
							expect(res.body.data).to.be.an('array').with.length(1)
							done()
						})
			})
			it('should return return bad request if limit is no an integer', (done) => {
				request
						.get('/events/')
						.set('Content-type', 'application/vnd.api+json')
						.query({ limit: 'abc' })
						.expect(BAD_REQUEST.code, done)
			})
		})
		describe('GET /Events/:id', () => {
			const testEvent = new Event({
				title: '/Events/:id test event',
				status: 'ACTIVE',
				description: 'Test Event, please Ignore',
				startDate: generateStartDate(),
				endDate: generateEndDate(),
				creator: 'TestUser',
				location: 'ChIJN1t_tDeuEmsRUsoyG83frY4'
			})

			before(function() {
				return co(function* () {
					try {
						yield initDb()
						yield Location.remove()
						yield Event.remove()
						yield new Location({_id: 'ChIJN1t_tDeuEmsRUsoyG83frY4'}).save()
						yield testEvent.save()
						yield testEvent.populate('location').execPopulate()
					} catch (err) {
						console.log(err)
						throw err
					}
				})
			})
			after(function() {
				return co(function* () {
					yield Event.remove()
					yield Location.remove()
				})
			})
			describe('success', function() {
				let response = null

				it('should return 200 OK', function(done) {
					request
						.get(`/events/${testEvent._id}`)
						.set('Content-type', 'application/vnd.api+json')
						.expect(OK.code, (err, res) => {
							if (err) {
								return done(err)
							}
							response = res
							done()
						})
				})
				it('should contain a top-level links property and a self link', function() {
					expect(response.body.links).to.not.be.null
					expect(response.body.links).to.be.an('object')
					expect(response.body.links).to.have.property('self')
					expect(response.body.links.self).to.equal(`https://localhost:3000/events/${testEvent._id}`)
				})
				it('should contain a top-level data object', function() {
					expect(response.body).to.have.property('data')
					expect(response.body.data).to.be.an('object')
					expect(response.body.data).to.not.be.null
				})
				describe('data item', function() {
					it('should be of type \'events\'', () => {
						expect(response.body.data).to.have.property('type')
						expect(response.body.data.type).to.be.a('string')
						expect(response.body.data.type).to.equal('events')
					})
					it('should contain all attributes', () => {
						expect(response.body.data).to.have.property('attributes').which.is.an('object')
						expect(response.body.data.attributes).to.have.property('title').which.equals(testEvent.title)
						expect(response.body.data.attributes).to.have.property('description').which.equals(testEvent.description)
						expect(response.body.data.attributes).to.have.property('startDate')
						expect(response.body.data.attributes).to.have.property('endDate')
						expect(response.body.data.attributes).to.have.property('creator').which.equals(testEvent.creator)
					})
					it('should have an object: relationships.location.links.related which is a path to the specific location resource', () => {
						expect(response.body.data).to.have.property('relationships').which.is.an('object')
						expect(response.body.data.relationships).to.have.property('location').which.is.an('object')
						expect(response.body.data.relationships.location).to.have.property('data').which.is.an('object')
						expect(response.body.data.relationships.location.data).to.have.property('type').which.is.a('string').which.equals('locations')
						expect(response.body.data.relationships.location.data).to.have.property('id').which.equals(testEvent.location._id)
					})
				})
				it('should return 304 Not Modified if event was modified BEFORE for if-modified-since ', (done) => {
					const dateOffset = 1000000

					request
						.get(`/events/${testEvent._id}`)
						.set('Content-type', 'application/vnd.api+json')
						.set('if-modified-since', new Date(Date.now() + dateOffset))
						.expect(NOT_MODIFIED.code, done)
				})
				it('should return 200 OK if event was modified AFTER for if-modified-since ', (done) => {
					const dateOffset = 1000000

					request
						.get(`/events/${testEvent._id}`)
						.set('Content-type', 'application/vnd.api+json')
						.set('if-modified-since', new Date(Date.now() - dateOffset))
						.expect(OK.code, done)
				})
			})

			describe('error', function() {
				it('should return 400 BAD_REQUEST if the content-type header is not set to \'application/vnd.api+json\' ', (done) => {
					request
						.get(`/events/${testEvent._id}`)
						.expect(BAD_REQUEST.code, done)
				})
				it('should return 404 NOT_FOUND if the requested event does not exist', (done) => {
					request
						.get('/events/dsjasjjhashasj')
						.set('content-type', 'application/vnd.api+json')
						.expect(NOT_FOUND.code, done)
				})
			})

		})
		describe('POST /Events', () => {
			const creator = new User({ username: 'testy123', password: 'password123' })
			let response = null
			let authHeaderVal = null
			const validPayload = {
				'data': {
					'type': 'events',
					'attributes': {
						'title': 'A Test Event',
						'status': 'ACTIVE',
						'description': 'Test Event, please Ignore',
						'startDate': generateStartDate(),//'Fri Dec 09 2016 01:40:30 GMT+0000 (GMT)',
						'endDate': generateEndDate()//'Fri Dec 09 2016 01:45:30 GMT+0000 (GMT)'
					},
					'relationships': {
						'location': {
							'data': {'type': 'locations', 'id': 'ChIJN1t_tDeuEmsRUsoyG83frY4'}
						}
					}
				}
			}

			before(function() {
				return co(function* () {
					try {
						yield initDb()
					} catch(err) {
						console.log(err)
						throw err
					}
				})
			})
			describe('success', function() {
				// Do request in the before function, cleanup in after
				before(function() {
					return co(function* () {
						try {
							const ctx = {request: { body: { username: 'testy123', password: 'password123' }}}

							yield User.remove()
							yield creator.save()
							yield AuthController.login(ctx)
							authHeaderVal = ctx.body.token
							console.log(authHeaderVal)
						} catch(err) {
							console.log(err)
						}
					})
				})
				it('should return 201 CREATED', function(done) {
					request.post('/events')
						.set('Content-type', 'application/vnd.api+json')
						.set('authorization', `Bearer ${authHeaderVal}`)
						.send(validPayload)
						.expect(CREATED.code, (err, res) => {
							if (err) {
								return done(err)
							}
							response = res
							done()
						})
				})
				it('should return a location header pointing to the resource', function() {
					expect(response.headers['location'])
				})
				it('should return an document containing the newly created resource', function() {
					expect(response.body).to.be.an('object')
					expect(response.body).to.not.be.null
				})
				it('should contain a data object with a type, id, attributes and location relationship', function() {
					expect(response.body.data).to.be.an('object')
					expect(response.body.data).to.have.property('type').which.equals('events')
					expect(response.body.data).to.have.property('id')
					expect(response.body.data).to.have.property('attributes').which.has.property('title')
					expect(response.body.data).to.have.property('attributes').which.has.property('description')
					expect(response.body.data).to.have.property('attributes').which.has.property('startDate')
					expect(response.body.data).to.have.property('attributes').which.has.property('endDate')
					expect(response.body.data).to.have.property('attributes').which.has.property('creator').which.equals(creator.username)
					expect(response.body.data).to.have.property('relationships').which.has.property('location')
						.which.has.property('data').which.has.property('type').which.equals('locations')
					expect(response.body.data).to.have.property('relationships').which.has.property('location')
											.which.has.property('data').which.has.property('id').which.equals('ChIJN1t_tDeuEmsRUsoyG83frY4')
				})
				it('the self link should match the location header', () => {
					expect(response.headers['location']).to.equal(response.body.links.self)
				})
			})

			describe('error', function() {
				it('should return 400 BAD_REQUEST if payload is empty', function(done) {
					request.post('/events')
						.set('Content-type', 'application/vnd.api+json')
						.set('authorization', `Bearer ${authHeaderVal}`)
						.send()
						.expect(BAD_REQUEST.code, done)
				})
				it('should return 400 BAD_REQUEST if the client does not send header Content-Type: application/vnd.api+json', (done) => {
					request.post('/events')
						.set('authorization', `Bearer ${authHeaderVal}`)
						.send(validPayload)
						.expect(BAD_REQUEST.code, done)
				})
				it('should return 400 BAD_REQUEST if payload does not have a data property containing a document', function(done) {
					const payload = {
						'data': null
					}

					request.post('/events')
						.set('Content-type', 'application/vnd.api+json')
						.set('authorization', `Bearer ${authHeaderVal}`)
						.send(payload)
						.expect(BAD_REQUEST.code, done)
				})
				it('should return 400 BAD_REQUEST if document does not have type \'events\' ', function(done) {
					const payload = merge({}, validPayload)

					payload.data.type = null

					request.post('/events')
						.set('Content-type', 'application/vnd.api+json')
						.set('authorization', `Bearer ${authHeaderVal}`)
						.send(payload)
						.expect(BAD_REQUEST.code, done)
				})
				it('should return 400 BAD_REQUEST if document has an id property', function(done) {
					const payload = merge({}, validPayload)

					payload.data.id = 123
					request.post('/events')
						.set('Content-type', 'application/vnd.api+json')
						.set('authorization', `Bearer ${authHeaderVal}`)
						.send(payload)
						.expect(BAD_REQUEST.code, done)
				})
				it('should return 400 BAD_REQUEST if the document does not contain all required attributes', function(done) {
					const payload = merge({}, validPayload)

					payload.data.attributes = null

					request.post('/events')
						.set('Content-type', 'application/vnd.api+json')
						.set('authorization', `Bearer ${authHeaderVal}`)
						.send(payload)
						.expect(BAD_REQUEST.code, done)
				})
				it('should return 400 BAD_REQUEST if the endDate is before the startDate', function(done) {
					const payload = merge({}, validPayload)

					payload.data.attributes.startDate = generateEndDate()
					payload.data.attributes.endDate = generateStartDate()

					request.post('/events')
						.set('Content-type', 'application/vnd.api+json')
						.set('authorization', `Bearer ${authHeaderVal}`)
						.send(payload)
						.expect(BAD_REQUEST.code, done)
				})
				it('should return 400 BAD_REQUEST if location relationship is missing', function(done) {
					const payload = merge({}, validPayload)

					payload.data.relationships = null

					request.post('/events')
						.set('Content-type', 'application/vnd.api+json')
						.set('authorization', `Bearer ${authHeaderVal}`)
						.send(payload)
						.expect(BAD_REQUEST.code, done)
				})
				it('should return 400 BAD_REQUEST if the location specified does not exist', function(done) {
					const payload = merge({}, validPayload)

					payload.data.relationships.location.data.id = 'asdfa'

					request.post('/events')
						.set('Content-type', 'application/vnd.api+json')
						.set('authorization', `Bearer ${authHeaderVal}`)
						.send(payload)
						.expect(BAD_REQUEST.code, done)
				})
				it('should return 401 UNAUTHORIZED if the authentication header is missing', function(done) {
					request.post('/events')
						.set('Content-type', 'application/vnd.api+json')
						.send(validPayload)
						.expect(UNAUTHORIZED.code, done)
				})
				it('should return 401 UNAUTHORIZED if the authentication header is incorrect', function(done) {
					request.post('/events')
						.set('Content-type', 'application/vnd.api+json')
						.set('authorization', 'Bearer jhadsjlfjfdk')
						.send(validPayload)
						.expect(UNAUTHORIZED.code, done)
				})
				it('should return 401 UNAUTHORIZED if the auth token is expired', (done) => {
					const payload = merge({}, validPayload)
					const headerval = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoidGVzdHkxMjMiLCJpYXQiOjE0ODEyNDY2MTAsImV4cCI6MTQ4MTI0ODQxMH0.jPwS5RQfar3trg-ong81-iBBBb4TqcrUVTN4E5JQiBY'

					request.post('/events')
						.set('Content-type', 'application/vnd.api+json')
						.set('authorization', headerval)
						.send(payload)
						.expect(UNAUTHORIZED.code, done)
				})
			})
		})
		describe('PATCH /Events/:id', () => {
			const ctx = {request: { body: { username: 'TestUser', password: 'slkfjd' }}}
			const ctx2 = {request: { body: { username: 'TestUser2', password: 'slkfjd' }}}
			let validPayload
			let response
			let jwt
			let jwt2
			const originalData = {
				title: 'A Test Event',
				description: 'Test Event, please Ignore',
				startDate: generateStartDate(),
				endDate: generateEndDate(),
				creator: 'TestUser',
				location: 'ChIJN1t_tDeuEmsRUsoyG83frY4'
			}
			let testEvent = new Event(originalData)

			before(function() {
				return co(function*() {
					yield initDb()
					yield Promise.all([User.remove(), Event.remove(), Location.remove()])
					yield Promise.all([
						new User({username: 'TestUser', password: 'slkfjd'}).save(),
						new User({ username: 'TestUser2', password: 'slkfjd' }).save(),
						testEvent.save()
					])
					yield AuthController.login(ctx)
					yield AuthController.login(ctx2)
					jwt = ctx.body.token
					jwt2 = ctx2.body.token
					testEvent = yield testEvent.populate('location').execPopulate()
					validPayload = new EventSerializer(config.host).serializeSingle(testEvent)
				})
			})
			beforeEach(() => co(function* () {
				testEvent = yield Event.findByEventId(testEvent._id)
				testEvent = yield testEvent.populate('location').execPopulate()
			}))
			describe('success', function() {
				it('should return 200 OK', (done) => {
					const payload = merge({}, validPayload)

					payload.data.attributes.description = 'Updated'
					request
						.patch(`/events/${testEvent._id}`)
						.set('content-type', 'application/vnd.api+json')
						.set('authorization', `Bearer ${jwt}`)
						.send(payload)
						.expect(OK.code, (err, res) => {
							response = res
							if (err) {
								console.dir(err)
								return done(err)
							}
							expect(res.body.data.attributes.description).to.equal(payload.data.attributes.description)
							return done()
						})
				})
				it('should return a location header with a path to the resource', () => {
					expect(response.headers.location).to.equal(`https://${config.host}/events/${testEvent._id}`)
				})
				it('should contain a top-level links property with a self link to the resource', () => {
					expect(response.body).to.have.property('links')
					expect(response.body.links).to.eql({
						self: `https://${config.host}/events/${testEvent._id}`
					})
				})
				it('should have a top-level data object', () => {
					expect(response.body).to.have.property('data').which.is.an('object')
				})
				describe('data object', function() {
					describe('data item', function() {
						it('should have type of \'events\'', () => {
							expect(response.body.data).to.have.property('type')
								.which.equals('events')
						})
						it('should have an id property that matches the resource id', () => {
							expect(response.body.data).to.have.property('id')
								.which.equals(testEvent._id.toString())
						})
						// it('should have a links property and a self link', () => {
						// 	expect(response.body.data).to.have.property('links')
						// 		.which.eql({
						// 			self: `https://${config.host}/events/${testEvent._id}`
						// 		})
						// })
						it('should contain all attributes', () => {
							expect(response.body.data).to.have.property('attributes')
								.which.eql({
									title: testEvent.title,
									description: testEvent.description,
									startDate: testEvent.startDate.toISOString(),
									endDate: testEvent.endDate.toISOString(),
									creator: testEvent.creator
								})
						})
						it('should have an object: relationships.location.links.related which is a path to the specific location resource', () => {
							expect(response.body.data).to.have.property('relationships')
								.which.eql({
									location: {
										data: {
											type: 'locations',
											id: testEvent.location._id
										}
									}
								})
						})
					})
				})
			})

			describe('error', function() {
				it('should return 400 BAD_REQUEST if payload is empty', (done) => {
					request
						.patch(`/events/${testEvent._id}`)
						.set('content-type', 'application/vnd.api+json')
						.set('authorization', `Bearer ${jwt}`)
						.send()
						.expect(BAD_REQUEST.code, done)
				})
				it('should return 400 BAD_REQUEST if the client does not send header Content-Type: application/vnd.api+json', (done) => {
					request
						.patch(`/events/${testEvent._id}`)
						.set('authorization', `Bearer ${jwt}`)
						.send(validPayload)
						.expect(BAD_REQUEST.code, done)
				})
				it('should return 400 BAD_REQUEST if payload does not have a data property containing a document', (done) => {
					const payload = merge({}, validPayload)

					payload.data = {}
					request
						.patch(`/events/${testEvent._id}`)
						.set('content-type', 'application/vnd.api+json')
						.set('authorization', `Bearer ${jwt}`)
						.send(payload)
						.expect(BAD_REQUEST.code, done)
				})
				it('should return 400 BAD_REQUEST if document does not have type \'events\' ', (done) => {
					const payload = merge({}, validPayload)

					payload.data.type = ''
					request
						.patch(`/events/${testEvent._id}`)
						.set('content-type', 'application/vnd.api+json')
						.set('authorization', `Bearer ${jwt}`)
						.send(payload)
						.expect(BAD_REQUEST.code, done)
				})
				it('should return 400 BAD_REQUEST if the id property does not match the resource', (done) => {
					const payload = merge({}, validPayload)

					payload.data.id = 16546
					request
						.patch(`/events/${testEvent._id}`)
						.set('content-type', 'application/vnd.api+json')
						.set('authorization', `Bearer ${jwt}`)
						.send(payload)
						.expect(BAD_REQUEST.code, done)
				})
				it('should return 400 BAD_REQUEST if the id property does exist on the document ', (done) => {
					const payload = merge({}, validPayload)

					payload.data.id = null
					request
						.patch(`/events/${testEvent._id}`)
						.set('content-type', 'application/vnd.api+json')
						.set('authorization', `Bearer ${jwt}`)
						.send(payload)
						.expect(BAD_REQUEST.code, done)
				})
				it('should return 400 BAD_REQUEST if the document does not contain all required attributes', (done) => {
					const payload = merge({}, validPayload)

					payload.data.attributes = {}
					request
						.patch(`/events/${testEvent._id}`)
						.set('content-type', 'application/vnd.api+json')
						.set('authorization', `Bearer ${jwt}`)
						.send(payload)
						.expect(BAD_REQUEST.code, done)
				})
				it('should return 400 BAD_REQUEST if the endDate is before the startDate', (done) => {
					const payload = merge({}, validPayload)

					payload.data.attributes.startDate = generateEndDate()
					payload.data.attributes.endDate = generateStartDate()
					request
						.patch(`/events/${testEvent._id}`)
						.set('content-type', 'application/vnd.api+json')
						.set('authorization', `Bearer ${jwt}`)
						.send(payload)
						.expect(BAD_REQUEST.code, done)
				})
				it('should return 401 UNAUTHORIZED if no valid authorization header is sent', (done) => {
					request
						.patch(`/events/${testEvent._id}`)
						.set('content-type', 'application/vnd.api+json')
						.send(validPayload)
						.expect(UNAUTHORIZED.code, done)
				})
				it('should return 403 FORBIDDEN if the authenticated user is not the creator of the resource', (done) => {
					request
						.patch(`/events/${testEvent._id}`)
						.set('content-type', 'application/vnd.api+json')
						.set('authorization', `Bearer ${jwt2}`)
						.send(validPayload)
						.expect(FORBIDDEN.code, done)
				})
				it('should return 404 NOT_FOUND if the given event does not exist', (done) => {
					request
						.patch('/events/dsjasjjhashasj')
						.set('content-type', 'application/vnd.api+json')
						.set('authorization', `Bearer ${jwt2}`)
						.send(validPayload)
						.expect(NOT_FOUND.code, done)
				})
				it('should return 404 NOT_FOUND if the updated location does not exist', (done) => {
					const pLoad = merge({}, validPayload)

					pLoad.data.relationships.location.data.id = 'sdffjhk'
					request
						.patch(`/events/${testEvent._id}`)
						.set('content-type', 'application/vnd.api+json')
						.set('authorization', `Bearer ${jwt}`)
						.send(pLoad)
						.expect(NOT_FOUND.code, (err, res) => {
							if (err) return done(err)
							expect(res.body.errors[0].error).to.equal('NotFoundError')
							done()
						})
				})
			})
		})
		describe('GET /Events/:id/relationships/location', function() {
			const testEvent = new Event({
				title: '/Events/:id test event',
				status: 'ACTIVE',
				description: 'Test Event, please Ignore',
				startDate: generateStartDate(),
				endDate: generateEndDate(),
				creator: 'TestUser',
				location: 'ChIJN1t_tDeuEmsRUsoyG83frY4'
			})
			let response

			before(function() {
				return co(function* () {
					try {
						yield initDb()
						yield Location.remove()
						yield Event.remove()
						yield testEvent.save()
						yield testEvent.populate('location').execPopulate()
					} catch (err) {
						console.log(err)
						throw err
					}
				})
			})

			after(function() {
				return co(function* () {
					yield Event.remove()
					yield Location.remove()
				})
			})
			describe('success', () => {
				it('should return 200 OK with the location details of that event', (done) => {
					request.get(`/events/${testEvent._id}/relationships/location`)
						.set('Content-type', 'application/vnd.api+json')
						.expect(OK.code, (err, res) => {
							if (err) {
								return done(err)
							}
							response = res
							done()
						})
				})
				describe('response', function() {
					it('should contain a top-level data object with attributes, links', () => {
						expect(response.body).to.have.property('data')
						expect(response.body.data).to.have.property('attributes')
						expect(response.body.data).to.have.property('links')
					})
					describe('response.data', () => {
						it('should have type of \'locations\' ', () => {
							expect(response.body.data).to.have.property('type')
							expect(response.body.data.type).to.equal('locations')
						})
						it('should have property id', () => {
							expect(response.body.data).to.have.property('id')
								.which.equals(testEvent.location._id.toString())
						})
						it('should have attributes property with all attributes', () => {
							expect(response.body.data).to.have.property('attributes').which.is.an('object')
							expect(response.body.data.attributes).to.eql({
								name: testEvent.location.name,
								dateAdded: testEvent.location.dateAdded.toISOString(),
								latitude: testEvent.location.latitude,
								longitude: testEvent.location.longitude
							})
						})
						it('should have a link property with a self link to the reseource', () => {
							expect(response.body.data.links).to.eql({
								self: `https://${config.host}/locations/${testEvent.location._id}`
							})
						})
					})
					it('should contain a top-level links object with a self link to the relationship', () => {
						expect(response.body).to.have.property('links')
						expect(response.body.links).to.eql({
							self: `https://${config.host}/events/${testEvent._id}/relationships/location`,
							related: `https://${config.host}/events/${testEvent._id}`,
						})
					})
				})
			})

			describe('error', () => {
				it('should return 404 NOT_FOUND if the event :id does not exist', (done) => {
					request.get('/events/465456456/relationships/location')
						.set('Content-type', 'application/vnd.api+json')
						.expect(NOT_FOUND.code, done)
				})
			})

		})
		describe('GET /Events/:id/join', function() {
			let jwt
			const testUser = new User({ username: 'testy123', password: 'password123' })
			const ctx = {request: {body: {username: testUser.username, password: testUser.password}}}
			const testEvent = new Event({
				title: '/Events/:id test event',
				status: 'ACTIVE',
				description: 'Test Event, please Ignore',
				startDate: generateStartDate(),
				endDate: generateEndDate(),
				creator: 'TestUser',
				location: 'ChIJN1t_tDeuEmsRUsoyG83frY4'
			})

			before( () =>
				co(function* () {
					yield Promise.all([Event.remove(), User.remove()])
					yield Promise.all([testUser.save(), testEvent.save()])
					yield AuthController.login(ctx)
					jwt = ctx.body.token
				})
			)

			after( () =>
				co(function* () {
					yield Promise.all([Event.remove(), User.remove(), Location.remove()])
				})
			)

			it('should return 204 NO_CONTENT if successfully attending event', (done) => {
				request.get(`/events/${testEvent._id}/join`)
					.set('content-type', 'application/vnd.api+json')
					.set('authorization', `Bearer ${jwt}`)
					.expect(NO_CONTENT.code, done)
			})
			it('should return 204 NO_CONTENT if the user is already attending the event', (done) => {
				request.get(`/events/${testEvent._id}/join`)
					.set('content-type', 'application/vnd.api+json')
					.set('authorization', `Bearer ${jwt}`)
					.expect(NO_CONTENT.code, done)
			} )
			it('should return 404 NOT_FOUND if the event does not exist', (done) => {
				request.get('/events/114234/join')
					.set('content-type', 'application/vnd.api+json')
					.set('authorization', `Bearer ${jwt}`)
					.expect(NOT_FOUND.code, done)
			})
			it('should return UNAUTHORIZED if the user is not logged in', (done) => {
				request.get(`/events/${testEvent._id}/join`)
					.set('content-type', 'application/vnd.api+json')
					.expect(UNAUTHORIZED.code, done)
			})
		})
		describe('GET /Events/:id/leave', function() {
			let jwt
			const testUser = new User({ username: 'testy123', password: 'password123' })
			const ctx = {request: {body: {username: testUser.username, password: testUser.password}}}
			const testEvent = new Event({
				title: '/Events/:id test event',
				status: 'ACTIVE',
				description: 'Test Event, please Ignore',
				startDate: generateStartDate(),
				endDate: generateEndDate(),
				creator: 'TestUser',
				location: 'ChIJN1t_tDeuEmsRUsoyG83frY4',
				attending: ['testy123']
			})

			before( () =>
				co(function* () {
					yield Promise.all([Event.remove(), User.remove()])
					yield Promise.all([testUser.save(), testEvent.save()])
					yield AuthController.login(ctx)
					jwt = ctx.body.token
				})
			)

			after( () =>
				co(function* () {
					yield Promise.all([Event.remove(), User.remove(), Location.remove()])
				})
			)
			it('should return 204 NO_CONTENT if successfully un-attending the event', (done) => {
				request.get(`/events/${testEvent._id}/leave`)
					.set('content-type', 'application/vnd.api+json')
					.set('authorization', `Bearer ${jwt}`)
					.expect(NO_CONTENT.code, done)
			})
			it('should return 204 NO_CONTENT if the user is already not attending the event', (done) => {
				request.get(`/events/${testEvent._id}/leave`)
					.set('content-type', 'application/vnd.api+json')
					.set('authorization', `Bearer ${jwt}`)
					.expect(NO_CONTENT.code, done)
			})
			it('should return 404 NOT_FOUND if the event does not exist', (done) => {
				request.get('/events/114234/leave')
					.set('content-type', 'application/vnd.api+json')
					.set('authorization', `Bearer ${jwt}`)
					.expect(NOT_FOUND.code, done)
			})
			it('should return UNAUTHORIZED if the user is not logged in', (done) => {
				request.get(`/events/${testEvent._id}/leave`)
					.set('content-type', 'application/vnd.api+json')
					.expect(UNAUTHORIZED.code, done)
			})
		})
		describe('DELETE /Events/:id', () => {
			const ctx = {request: { body: { username: 'TestUser', password: 'slkfjd' }}}
			const ctx2 = {request: { body: { username: 'TestUser2', password: 'slkfjd' }}}
			let jwt, jwt2, testEvent

			before(function() {
				return co(function*() {
					yield initDb()
					yield Promise.all([User.remove(), Event.remove(), Location.remove()])
					yield Promise.all([new User({username: 'TestUser', password: 'slkfjd'}).save(), new User({ username: 'TestUser2', password: 'slkfjd' }).save()])
					yield AuthController.login(ctx)
					yield AuthController.login(ctx2)
					jwt = ctx.body.token
					jwt2 = ctx2.body.token
				})
			})


			beforeEach(() =>
				co(function* () {
					yield Event.remove()
					testEvent = new Event({
						title: 'A Test Event',
						description: 'Test Event, please Ignore',
						startDate: generateStartDate(),
						endDate: generateEndDate(),
						creator: 'TestUser',
						location: 'ChIJN1t_tDeuEmsRUsoyG83frY4'
					})
					yield testEvent.save()
				})
			)
			after(() =>
				co(function* () {
					yield Promise.all([User.remove(), Event.remove(), Location.remove()])
				})
			)
			describe('success', () => {
				it('should return 204 NO_CONTENT for a successful deletion', (done) => {
					request.delete(`/events/${testEvent._id}`)
						.set('content-type', 'application/vnd.api+json')
						.set('authorization', `Bearer ${jwt}`)
						.expect(NO_CONTENT.code, done)
				})
			})

			describe('error', () => {
				it('should return 400 BAD_REQUEST if the client does not send header Content-Type: application/vnd.api+json', (done) => {
					request.delete('/events')
						.set('authorization', `Bearer ${jwt}`)
						.expect(BAD_REQUEST.code, done)
				})
				it('should return 401 UNAUTHORIZED if no valid authorization header is sent', (done) => {
					request
						.delete(`/events/${testEvent._id}`)
						.set('content-type', 'application/vnd.api+json')
						.expect(UNAUTHORIZED.code, done)
				})
				it('should return 403 FORBIDDEN if the authenticated user is not the creator of the resource', (done) => {
					request
						.delete(`/events/${testEvent._id}`)
						.set('content-type', 'application/vnd.api+json')
						.set('authorization', `Bearer ${jwt2}`)
						.expect(FORBIDDEN.code, done)
				})
				it('should return 404 NOT_FOUND if the given event does not exist', (done) => {
					request
						.delete('/events/dsjasjjhashasj')
						.set('content-type', 'application/vnd.api+json')
						.set('authorization', `Bearer ${jwt}`)
						.expect(NOT_FOUND.code, done)
				})
			})
		})
	})

	describe('Model Tests', function() {
		const testEvent = new Event({
			title: 'A Test Event',
			description: 'Test Event, please Ignore',
			startDate: generateStartDate(),
			endDate: generateEndDate(),
			creator: 'TestUser',
			location: 'ChIJN1t_tDeuEmsRUsoyG83frY4'
		})

		before(function() {
			return co(function* () {
				//yield initDb()
				yield Event.remove()
			})
		})
		after(function() {
			return co(function* () {
				yield Event.remove()
			})
		})

		describe('save', () => {
			it('should not ensure location if the location property is a document', () => co(function* () {
				expect(testEvent.location).to.be.a('string')
				yield testEvent.save()
				yield testEvent.populate('location').execPopulate()
				expect(testEvent.location).to.be.instanceOf(Location)
				yield testEvent.save()
				expect(testEvent.location).to.be.instanceOf(Location)
			}))
		})

		describe('findByEventId', () => {
			it('should throw EventNotFoundError if id param is null', () => co(function* () {
				try {
					yield Event.findByEventId(null)
				} catch(err) {
					expect(err).to.be.instanceOf(EventNotFoundError)
				}
			}))
			it('should throw NotFoundError if event cannot be found', () => co(function* () {
				try {
					yield Event.findByEventId('584ffc1ae5832d57a2cac77e')
				} catch(err) {
					expect(err).to.be.instanceOf(EventNotFoundError)
				}
			}))
		})
	})

	describe('Utilities', function() {
		describe('EventSerializer', function() {
			const serializer = new EventSerializer('localhost:3000')
			const testEvent = new Event({
				title: 'A Test Event',
				description: 'Test Event, please Ignore',
				startDate: generateStartDate(),
				endDate: generateEndDate(),
				creator: 'TestUser',
				location: 'ChIJN1t_tDeuEmsRUsoyG83frY4'
			})
			const testEvent2 = new Event({
				title: 'A Test  sdfsadf asEvent',
				description: 'Test Eventfdsdf, please Ignore',
				startDate: generateStartDate(),
				endDate: generateEndDate(),
				creator: 'TestUser2',
				location: 'ChIJN1t_tDeuEmsRUsoyG83frY4'
			})

			before(function() {
				return co(function* () {
					try {
						yield initDb()
						yield Promise.all([Location.remove(), Event.remove()])
						yield new Location({_id: 'ChIJN1t_tDeuEmsRUsoyG83frY4'}).save()
						yield Promise.all([testEvent.save(), testEvent2.save()])
						yield Promise.all([testEvent.populate('location').execPopulate(), testEvent2.populate('location').execPopulate()])
					} catch (err) {
						console.log(err)
						throw err
					}
				})
			})
			describe('serializeSingle', () => {
				let serializedEvent = null

				 before(function() {
					try {
						serializedEvent = serializer.serializeSingle(testEvent)
						expect(serializedEvent).to.not.be.null
					} catch (err) {
						console.log(err)
						throw err
					}
				 })
				it('should have a top-level data object', function() {
					expect(serializedEvent).to.have.property('data').which.is.an('object')
				})
				it('should have top-level links object containing a self link to the resource', () => {
					expect(serializedEvent).to.have.property('links')
					expect(serializedEvent.links).to.not.be.null
					expect(serializedEvent.links).to.have.property('self')
					expect(serializedEvent.links.self).to.not.be.null
					expect(serializedEvent.links.self).to.equal(`https://localhost:3000/events/${testEvent._id}`)

				})
				it('should have properties id, and type of events in data', function() {
					expect(serializedEvent.data).to.have.property('type')
						.which.is.a('string')
						.and.equals('events')

					expect(serializedEvent.data).to.have.property('id')
				})
				it('should have all correct attributes', function() {
					expect(serializedEvent.data).to.have.property('attributes')
						.which.is.an('object')
					expect(serializedEvent.data.attributes).to.have.property('title')
					expect(serializedEvent.data.attributes).to.have.property('description')
					expect(serializedEvent.data.attributes).to.have.property('startDate')
					expect(serializedEvent.data.attributes).to.have.property('endDate')
				})
				it('should have the location relationship', function() {
					expect(serializedEvent.data).to.have.property('relationships')
						.which.is.an('object')
					expect(serializedEvent.data.relationships).to.have.property('location')
					expect(serializedEvent.data.relationships)
						.to.have.property('location')
						.which.has.a.property('data')
					expect(serializedEvent.data.relationships.location.data)
						.to.have.a.property('type')
						.which.equals('locations')
					expect(serializedEvent.data.relationships.location.data)
						.to.have.a.property('id')
						.which.equals('ChIJN1t_tDeuEmsRUsoyG83frY4')
				})
			})
			describe('serializeMultiple', () => {
				let serializedEvents = null

				before(function() {
					try {
						serializedEvents = serializer.serializeMultiple([testEvent, testEvent2])
						expect(serializedEvents).to.not.be.null
					} catch (err) {
						console.log(err)
						throw err
					}
				})
				it('should have top-level links object containing a self link to the root collection', () => {
					expect(serializedEvents).to.have.property('links')
					expect(serializedEvents.links).to.not.be.null
					expect(serializedEvents.links).to.have.property('self')
					expect(serializedEvents.links.self).to.not.be.null
					expect(serializedEvents.links.self).to.equal('https://localhost:3000/events/')

				})
				it('should have a top-level data with an array containing 2 event resources', () => {
					const arrLength = 2

					expect(serializedEvents.data).to.be.an('array').of.length(arrLength)
				})
				describe('Each individual data item', () => {
					it('should a links object with a self link to the resource', () => {
						expect(serializedEvents.data[0].links).to.include({
							self: `https://localhost:3000/events/${testEvent._id}`
						})
						expect(serializedEvents.data[1].links).to.include({
							self: `https://localhost:3000/events/${testEvent2._id}`
						})
					})
					it('should contain id and type of events', () => {
						expect(serializedEvents.data[0]).to.include({
							id: testEvent._id.toString(),
							type: 'events'
						})
						expect(serializedEvents.data[1]).to.include({
							id: testEvent2._id.toString(),
							type: 'events'
						})
					})
					it('should contain a relationships object referencing the location id', () => {
						expect(serializedEvents.data[0].relationships).to.eql({
							location: {
								data: {
									type: 'locations',
									id: testEvent.location._id
								}
							}
						})
						expect(serializedEvents.data[1].relationships).to.eql({
							location: {
								data: {
									type: 'locations',
									id: testEvent2.location._id
								}
							}
						})
					})
					it('should contain an attributes object with all correct data', () => {
						expect(serializedEvents.data[0]).to.have.any.key('attributes')
						expect(serializedEvents.data[1]).to.have.any.key('attributes')
						expect(serializedEvents.data[0].attributes).to.eql({
							title: testEvent.title,
							description: testEvent.description,
							startDate: testEvent.startDate,
							endDate: testEvent.endDate,
							creator: testEvent.creator
						})
						expect(serializedEvents.data[1].attributes).to.eql({
							title: testEvent2.title,
							description: testEvent2.description,
							startDate: testEvent2.startDate,
							endDate: testEvent2.endDate,
							creator: testEvent2.creator
						})
					})
				})
			})
			describe('deserialize', () => {
				const example = {
					data: {
						type: 'events',
						attributes: {
							title: 'A Test Event',
							status: 'ACTIVE',
							description: 'Test Event, please Ignore',
							startDate: 'Thu Dec 08 2016 14:51:27 GMT+0000 (GMT)',
							endDate: 'Thu Dec 08 2016 17:39:12 GMT+0000 (GMT)',
						},
						relationships: {
							location: {
								data: {type: 'locations', id: 'ChIJN1t_tDeuEmsRUsoyG83frY4'}
							}
						}
					}
				}

				it('should take a JSONAPI object and return a normal object', function() {
					return co(function* () {
						try {
							const result = yield serializer.deserialize(example)

							expect(result).to.be.an('object')
							expect(result).to.have.property('title')
								.which.equals(example.data.attributes.title)
							expect(result).to.have.property('status')
								.which.equals(example.data.attributes.status)
							expect(result).to.have.property('description')
								.which.equals(example.data.attributes.description)
							expect(result).to.have.property('startDate')
								.which.equals(example.data.attributes.startDate)
							expect(result).to.have.property('endDate')
								.which.equals(example.data.attributes.endDate)
							expect(result).to.have.property('location')
								.which.equals(example.data.relationships.location.data.id)
						} catch(err) {
							console.log(err)
							throw err
						}
					})
				})
			})
		})

		describe('validateEventPayload', function() {
			const validPayload = {
				body: {
					data: {
						type: 'events',
						attributes: {
							title: 'A Test Event',
							status: 'ACTIVE',
							description: 'Test Event, please Ignore',
							startDate: generateStartDate(),
							endDate: generateEndDate(),
						},
						relationships: {
							location: {
								data: {type: 'locations', id: 'ChIJN1t_tDeuEmsRUsoyG83frY4'}
							}
						}
					}
				}
			}

			it('should return flat object containing event data', function() {
				return co(function* () {
					try {
						const result = yield validateEventPayload(validPayload)

						expect(result).to.have.property('title').which.equals(validPayload.body.data.attributes.title)
						expect(result).to.have.property('description').which.equals(validPayload.body.data.attributes.description)
						expect(result).to.have.property('startDate').which.equals(validPayload.body.data.attributes.startDate)
						expect(result).to.have.property('endDate').which.equals(validPayload.body.data.attributes.endDate)
						expect(result).to.have.property('location').which.equals(validPayload.body.data.relationships.location.data.id)
					} catch(err) {
						console.log(err)
						throw err
					}
				})
			})
			it('should throw an InvalidArgumentError if request parameter is null', function() {
				const request = null

				return co(function* () {
					try {
						yield validateEventPayload(request)
						throw Error('Should have thrown an exception')
					} catch (err) {
						expect(err).to.be.an.instanceof(InvalidArgumentError)
					}
				})
			})
			it('should throw an InvalidEventError if request.body is null', function() {
				const request = {
					body: null
				}

				co(function* () {
					try {
						yield validateEventPayload(request)
						throw new Error('should have thron an exception')
					} catch(err) {
						expect(err).to.be.an.instanceof(InvalidEventError)
					}
				})
			})
			it('should throw InvalidEventError if title attribute missing', function() {
				const request = merge({}, validPayload)

				delete request.body.data.attributes.title
				return co(function* () {
					try {
						yield validateEventPayload(request)
						throw new Error('should have thrown an exception')
					} catch(err) {
						expect(err).to.be.an.instanceof(InvalidEventError)
					}
				})
			})
			it('should throw InvalidEventError if description attribute missing', function() {
				const request = merge({}, validPayload)

				delete request.body.data.attributes.description
				return co(function* () {
					try {
						yield validateEventPayload(request)
						throw new Error('should have thrown an exception')
					} catch(err) {
						expect(err).to.be.an.instanceof(InvalidEventError)
					}
				})
			})
			it('should throw InvalidEventError if startDate attribute missing', function() {
				const request = merge({}, validPayload)

				delete request.body.data.attributes.startDate
				return co(function* () {
					try {
						yield validateEventPayload(request)
						throw new Error('should have thrown an exception')
					} catch(err) {
						expect(err).to.be.an.instanceof(InvalidEventError)
					}
				})
			})
			it('should throw InvalidEventError if endDate attribute missing', function() {
				const request = merge({}, validPayload)

				delete request.body.data.attributes.endDate
				return co(function* () {
					try {
						yield validateEventPayload(request)
						throw new Error('should have thrown an exception')
					} catch(err) {
						expect(err).to.be.an.instanceof(InvalidEventError)
					}
				})
			})
			it('should throw an InvalidEventError if startDate is not a valid Date', function() {
				const request = merge({}, validPayload)

				request.body.data.attributes.startDate = 'ajkjkha'
				return co(function* () {
					try {
						yield validateEventPayload(request)
					} catch(err) {
						expect(err).to.be.an.instanceof(InvalidEventError)
					}
				})
			})
			it('should throw an InvalidEventError if endDate is not a valid Date', function() {
				const request = merge({}, validPayload)

				request.body.data.attributes.endDate = 'asdfas'
				return co(function* () {
					try {
						yield validateEventPayload(request)
					} catch(err) {
						expect(err).to.be.an.instanceof(InvalidEventError)
					}
				})
			})
			it('should throw an InvalidEventError if startDate is in the past', function() {
				const request = merge({}, validPayload)
				const bigNumber = 100000000

				request.body.data.attributes.startDate = new Date(Date.now() - bigNumber)
				return co(function* () {
					try {
						yield validateEventPayload(request)
					} catch(err) {
						expect(err).to.be.an.instanceof(InvalidEventError)
					}
				})
			})
			it('should throw an InvalidEventError if endDate is before startDate', function() {
				const request = merge({}, validPayload)

				request.body.data.attributes.startDate = generateEndDate()
				request.body.data.attributes.endDate = generateStartDate(0)
				return co(function* () {
					try {
						yield validateEventPayload(request)
					} catch(err) {
						expect(err).to.be.an.instanceof(InvalidEventError)
					}
				})
			})
			it('should throw an InvalidEventError if event contains erroneous attributes', () => {
				const request = merge({}, validPayload)

				request.body.data.attributes.a = '123'
				request.body.data.attributes.b = '123'
				request.body.data.attributes.c = '123'
				request.body.data.attributes.d = '123'
				return co(function* () {
					try {
						yield validateEventPayload(request)
					} catch(err) {
						expect(err).to.be.an.instanceof(InvalidEventError)
					}
				})
			})
		})
	})
})
