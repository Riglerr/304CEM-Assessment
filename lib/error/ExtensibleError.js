'use strict'

/**
 * @description Custom Error Base class
 *
 * @class ExtenisbleError
 * @extends {Error}
 */
class ExtensibleError extends Error {
	/**
	 * Creates an instance of ExtenisbleError.
	 *
	 * @param {any} message - Error Message
	 *
	 * @memberof ExtenisbleError
	 */
	constructor(message) {
		super(message)
		this.name = this.constructor.name
		this.message = message
	}
}

module.exports = ExtensibleError
