'use strict'

const NotFoundError = require('./NotFoundError')

/**
 *
 *
 * @class EventNotFound
 * @extends {NotFoundError}
 */
class EventNotFound extends NotFoundError {
	/**
	 * Creates an instance of EventNotFound.
	 *
	 * @param {any} id - The id if the event that could not be found
	 *
	 * @memberOf EventNotFound
	 */
	constructor(id) {
		super()
		this.message = `An Event with the id: ${id} does not exist.`
	}
}

module.exports = EventNotFound
