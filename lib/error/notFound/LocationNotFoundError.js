'use strict'

const NotFoundError = require('./NotFoundError')


/**
 *
 *
 * @class LocationNotFound
 * @extends {NotFoundError}
 */
class LocationNotFound extends NotFoundError {
	/**
	 * Creates an instance of LocationNotFound.
	 *
	 * @param {any} id - The id of the location that could not be found
	 *
	 * @memberOf LocationNotFound
	 */
	constructor(id) {
		super()
		this.message = `A location with the id: ${id} does not exist.`
	}
}

module.exports = LocationNotFound
