'use strict'

// Base Error
const ExtensibleError = require('../ExtensibleError')
const {NOT_FOUND} = require('status-codes')

ExtensibleError
/**
 * @description An Error used to signify a resourse was not found.
 *
 * @class NotFoundError
 * @extends {Error}
 */
class NotFoundError extends ExtensibleError {
	/**
	 * Creates an instance of NotFoundError.
	 *
	 * @param {any} message - The Error message
	 *
	 * @memberOf NotFoundError
	 */
	constructor() {
		super()
		this.name = 'NotFoundError'
		this.statusCode = NOT_FOUND.code
	}
}

module.exports = NotFoundError

