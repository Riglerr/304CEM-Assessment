'use strict'

const NotFoundError = require('./NotFoundError')

/**
 *
 *
 * @class UserNotFound
 * @extends {NotFoundError}
 */
class UserNotFound extends NotFoundError {
	/**
	 * Creates an instance of UserNotFound.
	 *
	 * @param {any} username - The username that could not be found.
	 *
	 * @memberOf UserNotFound
	 */
	constructor(username) {
		super()
		this.message = `A user with the username: ${username} was not found`
	}
}

module.exports = UserNotFound
