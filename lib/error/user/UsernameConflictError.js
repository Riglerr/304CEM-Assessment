'use strict'

const ExtensibleError = require('../ExtensibleError')
const {CONFLICT} = require('status-codes')

/**
 *
 *
 * @class UsernameConflictError
 * @extends {ExtensibleError}
 */
class UsernameConflictError extends ExtensibleError {
	/**
	 * Creates an instance of UsernameConflictError.
	 *
	 * @param {string} username - The conflicting username
	 *
	 * @memberOf UsernameConflictError
	 */
	constructor(username) {
		super()
		this.name = 'UsernameConflictError'
		this.message = `The username: ${username} has already been registered`
		this.statusCode = CONFLICT.code
	}
}

module.exports = UsernameConflictError
