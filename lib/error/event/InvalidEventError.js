'use strict'

const ExtensibleError = require('../ExtensibleError')
const { BAD_REQUEST } = require('status-codes')


/**
 * Error thrown for invalid event data
 *
 * @class InvalidEventError
 * @extends {ExtensibleError}
 */
class InvalidEventError extends ExtensibleError {
	/**
	 * Creates an instance of InvalidEventError.
	 *
	 * @param {string} message - error message
	 */
	constructor(message) {
		super(message)
		this.statusCode = BAD_REQUEST.code
	}
}

module.exports = InvalidEventError
