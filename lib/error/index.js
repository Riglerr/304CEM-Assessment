'use strict'

const IncorrectPasswordError = require('./auth/IncorrectPasswordError')
const InvalidAuthorizationType = require('./auth/InvalidAuthorizationType')
const InvalidCredentialsError = require('./auth/InvalidCredentialsError.js')
const UnauthorizedError = require('./auth/UnauthorizedError.js')
const NotFoundError = require('./notFound/NotFoundError.js')
const UserNotFoundError = require('./notFound/UserNotFoundError.js')
const UsernameConflictError = require('./user/UsernameConflictError.js')
const ExtensibleError = require('./ExtensibleError.js')
const InvalidArgumentError = require('./InvalidArgumentError.js')
const InvalidLocationQueryError = require('./location/InvalidLocationError')
const EventNotFoundError = require('./notFound/EventNotFoundError')
const BadRequest = require('./BadRequest')

module.exports = {
	IncorrectPasswordError,
	InvalidAuthorizationType,
	InvalidCredentialsError,
	UnauthorizedError,
	NotFoundError,
	UserNotFoundError,
	UsernameConflictError,
	ExtensibleError,
	InvalidArgumentError,
	InvalidLocationQueryError,
	EventNotFoundError,
	BadRequest
}
