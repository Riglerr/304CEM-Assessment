'use strict'

const ExtensibleError = require('./ExtensibleError')
const { BAD_REQUEST } = require('status-codes')

/**
 * Generic BadRequest Error
 *
 * @class BadRequest
 * @extends {ExtensibleError}
 */
class BadRequest extends ExtensibleError {
	/**
	 * Creates an instance of BadRequest.
	 *
	 * @param {any} message - Reason for BadRequest
	 *
	 * @memberOf BadRequest
	 */
	constructor(message) {
		super(message)
		this.statusCode = BAD_REQUEST.code
	}
}

module.exports = BadRequest
