'use strict'

const ExtensibleError = require('../ExtensibleError')
const { BAD_REQUEST } = require('status-codes')

/**
 * Error used
 *
 * @class InvalidLocationError
 * @extends {ExtensibleError}
 */
class InvalidLocationQueryError extends ExtensibleError {
	/**
	 * Creates an instance of InvalidLocationError.
	 *
	 * @param {string} message - Location query error message
	 *
	 * @memberOf InvalidLocationError
	 */
	constructor(message) {
		super(message)
		this.statusCode = BAD_REQUEST.code
	}
}

module.exports = InvalidLocationQueryError
