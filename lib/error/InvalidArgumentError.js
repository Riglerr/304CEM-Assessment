'use strict'
const ExtensibleError = require('./ExtensibleError')

/**
 * @description Used for invalid function arguments
 *
 * @class InvalidArgumentError
 * @extends {ExtensibleError}
 */
class InvalidArgumentError extends ExtensibleError {
	/**
	 * Creates an instance of InvalidArgumentError.
	 *
	 * @param {any} argument -
	 * @param {any} reason -
	 *
	 * @memberOf InvalidArgumentError
	 */
	constructor(argument, reason) {
		super()
		this.name = 'InvalidArgumentError'
		this.message = `Invalid Argument: ${argument}, ${reason}`
	}
}

module.exports = InvalidArgumentError
