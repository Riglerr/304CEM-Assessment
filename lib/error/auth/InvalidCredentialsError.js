'use strict'


// Base Error
const ExtensibleError = require('../ExtensibleError')

const {BAD_REQUEST} = require('status-codes')

/**
 *
 *
 * @class InvalidCredentialsError
 * @extends {Error}
 */
class InvalidCredentialsError extends ExtensibleError {
	/**
	 * Creates an instance of InvalidCredentialsError.
	 *
	 * @param {any} message - Error Message
	 *
	 * @memberOf InvalidCredentialsError
	 */
	constructor(message) {
		super(message)
		this.message = 'Login Credentials Invalid'
		this.name = 'InvalidCredentialsError'
		this.statusCode = BAD_REQUEST.code
	}
}

module.exports = InvalidCredentialsError
