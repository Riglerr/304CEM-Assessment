'use strict'

const ExtensibleError = require('../ExtensibleError')
const {FORBIDDEN} = require('status-codes')

/**
 * Error for attempting to access forbidden reosurces
 *
 * @class ForbiddenError
 * @extends {ExtensibleError}
 */
class ForbiddenError extends ExtensibleError {
	/**
	 * Creates an instance of ForbiddenError.
	 *
	 * @param {any} message - Reason why ForbiddenError was thrown
	 *
	 * @memberOf ForbiddenError
	 */
	constructor(message) {
		super(message)
		this.statusCode = FORBIDDEN.code
	}
}

module.exports = ForbiddenError
