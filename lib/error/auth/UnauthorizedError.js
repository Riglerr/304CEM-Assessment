'use strict'

const ExtensibleError = require('../ExtensibleError')
const {UNAUTHORIZED} = require('status-codes')

/**
 * @description Authorization Error
 *
 * @class UnauthorizedError
 * @extends {ExtensibleError}
 */
class UnauthorizedError extends ExtensibleError {
	/**
	 * Creates an instance of UnauthorizedError.
	 *
	 * @param {string} message - Message for UnauthorizedError
	 * @memberOf UnauthorizedError
	 */
	constructor(message) {
		super(message)
		this.statusCode = UNAUTHORIZED.code
	}
}

module.exports = UnauthorizedError
