'use strict'

const ExtensibleError = require('../ExtensibleError')
const { UNAUTHORIZED } = require('status-codes')

/**
 * @description Error describing an incorrect authorization header type
 *
 * @class InvalidAuthorizationType
 * @extends {ExtensibleError}
 */
class InvalidAuthorizationType extends ExtensibleError {
	/**
	 * Creates an instance of InvalidAuthorizationType.
	 *
	 *
	 * @memberOf InvalidAuthorizationType
	 */
	constructor() {
		super()
		this.name = 'InvalidAuthorizationType'
		this.message = 'This Api requires a Bearer Authroization type.'
		this.statusCode = UNAUTHORIZED.code
	}
}

module.exports = InvalidAuthorizationType
