'use strict'

// Base Error
const ExtensibleError = require('../ExtensibleError')

// Custom status code utility
const {UNPROCESSABLE_ENTITY} = require('status-codes')

/**
 *
 *
 * @class IncorrectPasswordError
 * @extends {Error}
 */
class IncorrectPasswordError extends ExtensibleError {
	/**
	 * Creates an instance of IncorrectPasswordError.
	 *
	 * @param {any} message - Error message
	 *
	 * @memberOf IncorrectPasswordError
	 */
	constructor(message) {
		super(message)
		this.name = 'IncorrectPasswordError'
		this.message = 'Password field incorrect'
		this.statusCode = UNPROCESSABLE_ENTITY.code
	}
}

module.exports = IncorrectPasswordError
