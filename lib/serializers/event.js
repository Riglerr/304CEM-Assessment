'use strict'

const JSONAPI = require('jsonapi-serializer')
const defaultOptions = {
	id: 'id',
	attributes: ['title', 'description', 'status', 'startDate', 'endDate', 'creator', 'location'],
	location: { ref: 'id', included: false },
	keyForAttribute: 'camelCase'
}

/**
 *
 *
 * @class EventSerializer
 */
class EventSerializer {
	/**
	 * Creates an instance of EventSerializer.
	 *
	 * @param {any} host - current host. e.g localhost:3000 or shrouded-beach.heroku.com
	 *
	 * @memberOf EventSerializer
	 */
	constructor(host) {
		this.rootUrl = `https://${host}/events/`
		/**
		 *  One Ring to rule them all,
		 *	One Ring to find them,
		 *	One Ring to bring them all,
		 *	And in the darkness bind them.
		 */
		this.serializeSingle = this.serializeSingle.bind(this)
		this.serializeMultiple = this.serializeMultiple.bind(this)

	}

	/**
	 * @name serializeSingle
	 * @description Serializes a single event into a JSONAPI resource, with a top-level link to the event
	 * @param {Object} event - The Event to serialize
	 * @returns {Object} - The JSONAPI resource
	 *
	 * @memberOf EventSerializer
	 */
	serializeSingle(event) {
		const links = {
			topLevelLinks: {
				self: (event) => {
					let id = null

					if (Array.isArray(event)) {
						id = event[0].id
					} else {
						id = event.id
					}
					return `${this.rootUrl}${id}`
				}
			}
		}

		return new JSONAPI.Serializer('events', Object.assign({}, defaultOptions, links)).serialize(event)
	}

	/**
	 * @name serializeMultiple
	 * @description Serialized an array of events into a JSONAPI resource,
	 *  with top-level links to the collection root,
	 *  and data-level links to the individual resource
	 * @param {Array<object>} events - Array of events to serialize
	 * @returns {object} The JSONAPI resourse
	 *
	 * @memberOf EventSerializer
	 */
	serializeMultiple(events) {
		const links = {
			topLevelLinks: {
				self: () => `${this.rootUrl}`
			},
			dataLinks: {
				self: (dataSet, event) => `${this.rootUrl}${event.id}`
			}
		}

		return new JSONAPI.Serializer('events', Object.assign({}, defaultOptions, links)).serialize(events)
	}

	/**
	 * @name deserialize
	 * @description Deserialzes a JSONAPI event resource into a flat, mongoose compatible object.
	 * @param {any} events - The JSONAPI event resource to deserialize
	 * @returns {Object} - A flat, mongoose compatible object containing the event data.
	 *
	 * @memberOf EventSerializer
	 */
	deserialize(events) {
		return new JSONAPI.Deserializer({
			keyForAttribute: 'camelCase',
			locations: {
				valueForRelationship: relationship => relationship.id
			},
		}).deserialize(events)
	}
}

module.exports = EventSerializer
