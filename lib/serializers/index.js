'use strict'

const LocationSerializer = require('./location')
const EventSerializer = require('./event')

module.exports = {
	LocationSerializer,
	EventSerializer
}
