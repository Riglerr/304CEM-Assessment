'use strict'

const typeOf = require('kind-of')
const { InvalidArgumentError, InvalidLocationQueryError } = require('custom-errors')

/**
 * @name validateLocationQuery
 * @description - Validates a request query object used for the location endpoint,
 * and returns the data if successful
 * @param {Object} request - Koa Request
 * @returns {Object} - An object containing the location query data.
 */
module.exports = (request) => {
	if (!request || typeOf(request) !== 'object') throw new InvalidArgumentError('request', 'Request context wass null or not an object.')
	if (!request.query || typeOf(request.query) !== 'object') throw new InvalidArgumentError('request.query', 'query was null or not an object')
	if (!request.query.longitude) throw new InvalidLocationQueryError('longitude value cannot be null')
	if (!parseFloat(Math.abs(request.query.longitude))) throw new InvalidLocationQueryError(`longitude value ${request.query.longitude} is not valid,
		it must be a floating-point number`)
	if (!request.query.latitude) throw new InvalidLocationQueryError('latitude value cannot be null')

	if (!parseFloat(Math.abs(request.query.latitude))) throw new InvalidLocationQueryError(`latitude value ${request.query.latitude} is not valid,
		it must be a floating-point number`)
	if(request.query.keyword && typeOf(request.query.keyword) !== 'string') throw new InvalidLocationQueryError('keyword value is not a string')

	const result = {
		latitude: request.query.latitude,
		longitude: request.query.longitude
	}

	if (request.query.keyword) {
		result.keyword = request.query.keyword
	}
	return result
}
