'use strict'

/**
 * @description - Determines if a Number is an integer
 *
 * @param {any} value - The input value
 * @returns {Bool} - True if value is integer, false if not.
 */
module.exports = function(value) {
	return typeof value === 'number' &&
		isFinite(value) &&
		Math.floor(value) === value
}
