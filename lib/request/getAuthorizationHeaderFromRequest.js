'use strict'

const InvalidAuthorizationType = require('custom-errors/auth/InvalidAuthorizationType')

/**
 * @name getAuthorizationHeaderFromRequest
 * @description - Extracts and validates the Authorization header from a request context.
 *
 * @param {any} ctx - The Koa request context
 * @returns {String} - The Authorization header value.
 * @throws {InvalidAuthorizationType}
 */
module.exports = function(ctx) {
	if(!ctx) return null
	const authHeader = ctx.req.headers.authorization

	if (!authHeader) {
		return null
	}
	const containsBearer = authHeader.indexOf('Bearer') >= 0

	if (!containsBearer) {
		throw new InvalidAuthorizationType()
	}
	return RemoveBearerFromAuthHeader(authHeader)
}

/**
 * @description Removes type from Authorization header value
 *
 * @param {any} headerString - The Authorization Header
 * @returns {string} - Authorization header value minus the type.
 */
function RemoveBearerFromAuthHeader(headerString) {
	const divide = headerString.split('Bearer ')

	return divide.length > 1 ? divide[1] : divide[0]
}
