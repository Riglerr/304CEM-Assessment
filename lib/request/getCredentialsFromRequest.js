'use strict'

/**
 * @typedef {Object} Credential
 * @property {String} username - The users' username.
 * @property {String} password - The users' password.
 */

/**
 * @name getCredentialsFromRequest
 * @description Attempts to extract username & password from a request body.
 *
 * @param {Object} request - Node Request
 * @returns {Credential | null} - Object containing the username, password properties.
 */
module.exports = request => {
	if (!request || !request.body) {
		return null
	}
	const {username, password} = request.body

	return {
		username,
		password
	}
}
