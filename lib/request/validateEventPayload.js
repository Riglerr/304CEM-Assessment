'use strict'

const co = require('co')
const host = require('../../config').host
const InvalidArgumentError = require('custom-errors/InvalidArgumentError')
const InvalidEventError = require('custom-errors/event/InvalidEventError')
const EventSerializer = require('api-serializers/event')
const serializer = new EventSerializer(host)
const noOfKeys = 7

/**
 * @name validateEventPayload
 * @description Deserializes and validates a new Event Payload
 * @param {object} request - Koa Request Object
 * @param {Boolean} requireId - Determines if the evnt id property should be required (default false)
 * @param {object} resourceId - if requireId is true, then this value represents the linked event id
 * @returns {Promise<Object>} -
 * @throws {InvalidArgumentError} Argument request must not be null
 * @throws {InvalidEventError} request.body cannot be null or empty
 */
module.exports = (request, requireId = false, resourceId) => co(function* () {
	if (!request) throw new InvalidArgumentError('request', 'Request should not be null')
	if (!request.body || !request.body.data || Object.keys(request.body).length === 0) return null
	if (!request.body.data.type || request.body.data.type !== 'events')
		throw new InvalidEventError('data.type must be of type \'events\' ')

	if(requireId && !request.body.data.id) {
		throw new InvalidEventError('Id property required.')
	} else if(requireId && resourceId !== request.body.data.id) {
		throw new InvalidEventError('Resource Id does not match data id')
	} else if (!requireId && request.body.data.id) {
		throw new InvalidEventError('Request data must not have an id property')
	}
	const eventData = yield serializer.deserialize(request.body)

	if (!eventData.title) throw new InvalidEventError('title attribute required')
	if (!eventData.description) throw new InvalidEventError('description attribute required')
	if (!eventData.startDate) throw new InvalidEventError('startDate attribute required')
	if (!eventData.endDate) throw new InvalidEventError('endDate attribute required')
	const startDate = new Date(eventData.startDate)
	const endDate = new Date(eventData.endDate)

	if (startDate.toString() === 'Invalid Date') throw new InvalidEventError('startDate is not a valid date')
	if (endDate.toString() === 'Invalid Date') throw new InvalidEventError('endDate is not a valid date')
	if (startDate.valueOf() <= Date.now()) throw new InvalidEventError('startDate cannot be in the past')
	if (endDate.valueOf() < startDate.valueOf()) throw new InvalidEventError('endDate cannot be before startDate')

	if (!eventData.location) throw new InvalidEventError('location relationship required')
	if (Object.keys(eventData).length > noOfKeys) throw new InvalidEventError(''.concat('contained attributes that are misspelled or not valid.',
		'valid attributes are: title, description, startDate, endDate, location'))

	return eventData
})
