'use strict'

/**
 * @description Extracts the id field value from a request body.
 *
 * @param {any} params - Koa Request context
 * @returns {Number} - The value of the id field.
 */
module.exports = function getIdFromRequest(params) {
	if (!params) {
		return null
	}
	const id = params.id

	if (!id) {
		throw Error('BadRequest')
	}
	return id
}
