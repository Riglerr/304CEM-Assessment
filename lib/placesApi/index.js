'use strict'

// Utilities
const getJson = require('get-json')

// Default request options
const options = {
	host: 'maps.googleapis.com',
	method: 'GET',
	port: 443,
	headers: {
		'Content-Type': 'application/json'
	}
}

/**
 * @description Controls http interactions with the Google Places Api
 *
 * @class PlacesApi
 */
class PlacesApi {
	/**
	 * Creates an instance of PlacesApi.
	 *
	 * @param {string} apiKey - Google Places api key
	 *
	 * @memberof PlacesApi
	 */
	constructor(apiKey) {
		this.apiKey = apiKey
		this.nearbySearch = this.nearbySearch.bind(this)
		this.__buildPath = this.__buildPath.bind(this)
	}

	/**
	 *
	 * @description Retrieves an array of places using the google nearbysearch api.
	 * @param {string} latitude - The latitide
	 * @param {string} longitude - The Longitude
	 * @param {string} radius - The radius
	 * @param {string} keyword - (Optional) keyword to filter the results by
	 * @returns {Promise<Array>} - An Array of places
	 *
	 * @memberof PlacesApi
	 */
	nearbySearch(latitude, longitude, radius, keyword) {
		return this.__ApiRequest(this.__buildPath({lat: latitude, long: longitude, radius, keyword}))
	}

	/**
	 * @description Retrieves details about a specific place using the google details api
	 * @param {string} id - placeid of the place
	 * @returns {Promise<Array>} - An array of places
	 *
	 * @memberof PlacesApi
	 */
	details(id) {
		return this.__ApiRequest(this.__buildPath({id}))
	}

	/**
	 *
	 * @description Makes an HTTP request to the google places api
	 * @param {any} path - The path to request
	 * @returns {Promise<Array>} An array of places
	 *
	 * @memberof PlacesApi
	 */
	__ApiRequest(path) {
		return new Promise((resolve, reject) => {
			const searchOptions = Object.assign({}, options, { path })

			getJson(searchOptions)
			.then((jsonData) => {
				if (!jsonData) {
					return reject('No Data')
				}
				if (jsonData.status !== 'OK') {
					return reject(jsonData)
				}
				if (jsonData.results) {
					return resolve(jsonData.results)
				} else if(jsonData.result) {
					return resolve(jsonData.result)
				} else {
					return resolve(jsonData)
				}
			})
			.catch((err) => {
				reject(err)
			})
		})
	}
	/**
	 *
	 * @description Creates a request Path for the places API
	 * @param {Object} data - Path options
	 * @param {string} data.lat - Latitude
	 * @param {string} data.long - Longitude
	 * @param {string} data.radius - radius
	 * @param {string} data.keyword - keyword
	 * @param {string} data.id - placeid
	 * @returns {string} - A google places api request path
	 *
	 * @memberof PlacesApi
	 */
	__buildPath(data) {
		const {lat, long, radius, keyword, id} = data
		const apiKeyPath = `&key=${this.apiKey}`
		let result
		const staticPath = '/maps/api/place/'

		if (id) {
			result =
				`${staticPath}details/json?placeid=${id}${apiKeyPath}`
		} else {
			if (!lat || !long) {
				throw new Error('Invalid lat/long')
			}
			if (!radius) {
				throw new Error('Invalid radius')
			}
			if (keyword && typeof keyword !== 'string') {
				throw new Error('Invalid keyword')
			}
			const keywordPath = !keyword ? '' : `&keyword=${keyword}`

			result =
				`${staticPath}nearbysearch/json?location=${lat},${long}&radius=${radius}${keywordPath}${apiKeyPath}`
		}

		return result
	}
}

module.exports = PlacesApi
