'use strict'
const http = require('http')
const https = require('https')
const httpsPort = 443

/**
 * @name getJson
 * @description   REST get request returning JSON object(s)
 * @param {Object} options: http options object
 * @returns {Promise<Object>} JSON response from request
 */
function getJson(options) {
	return new Promise((resolve, reject) => {
		const port = options.port === httpsPort ? https : http

		options.method = 'GET'
		const req = port.request(options, function(res) {
			let output = ''

			res.setEncoding('utf8')

			res.on('data', function(chunk) {
				output += chunk
			})

			res.on('end', function() {
				const obj = JSON.parse(output)

				resolve(obj)
			})
		})

		req.on('error', function(err) {
			reject(err)
		})

		req.end()
	})
}

module.exports = getJson
