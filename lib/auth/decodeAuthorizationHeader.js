'use strict'

const jwt = require('jsonwebtoken')
const jwtSecret = require('../../config').jwtSecret

/**
 * @name decodeAuthorizationHeader
 * @description verifies the JWT contained in the authorization header.
 * @param {String} authHeader - The encrypted JWT
 * @return {Promise<object>} - The decoded JWT obejct.
 */
module.exports = authHeader =>
	new Promise((resolve, reject) => {
		jwt.verify(authHeader, jwtSecret, (err, decoded) => {
			if (err) {
				return reject(err)
			}
			resolve(decoded)
		})
	})
