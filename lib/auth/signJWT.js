'use strict'
const jwt = require('jsonwebtoken')
const co = require('co')
const secret = require('../../config').jwtSecret
const Promise = require('bluebird')
const signJwt = Promise.promisify(jwt.sign)
/**
 * @name signJwt
 * @description (Async)Creates a JsonWebToken that is a valid for 30 minutes.
 *
 * @param {any} user - The user to sign the key against.
 * @returns {Promise<String>} The JsonWebToken
 */

module.exports = user =>
	co(function* () {
		return yield signJwt(
			{user: user.username},
			secret,
			{expiresIn: '30m'})
	})

