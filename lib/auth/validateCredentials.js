'use strict'

const InvalidCredentialsError = require('custom-errors/auth/InvalidCredentialsError')

/**
 * @description - Validates a credentials object.
 *
 * @param {Credentials} credentials - The Credentials to validate.
 * @returns {Credentials} - An object containing the username and password.
 * @throws {InvalidCredentialsError}
 */
module.exports = (credentials) => {
	const {username, password} = credentials

	if (!username || !password) {
		throw new InvalidCredentialsError()
	}
	return {
		username,
		password
	}
}
