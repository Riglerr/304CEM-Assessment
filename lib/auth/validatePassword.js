'use strict'

const co = require('co')

const IncorrectPasswordError = require('custom-errors/auth/IncorrectPasswordError')
const InvalidArgumentError = require('custom-errors/InvalidArgumentError')

/**
 * @description Validates a password against a users stored password.
 *
 * @param {any} user - The user to validate against.
 * @param {any} passwordAttempt - The password to validate.
 * @returns {Promise<bool>} - Password validation result.
 * @throws {IncorrectPasswordError}
 */
function* validatePassword(user, passwordAttempt) {
	if (!user || typeof user.matchPasswordWithHash !== 'function') {
		throw new InvalidArgumentError('user', 'user is incorrect type or undefined.')
	}
	const passwordMatch = yield user.matchPasswordWithHash(passwordAttempt)

	if (!passwordMatch) {
		throw new IncorrectPasswordError()
	}
	return passwordMatch
}

module.exports = co.wrap (validatePassword)
