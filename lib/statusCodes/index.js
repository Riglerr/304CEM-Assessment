'use strict'
module.exports = {
	// success codes: 2.x
	OK: {
		code: 200,
		message: 'OK'
	},
	CREATED: {
		code: 201,
		message: 'Created'
	},
	NO_CONTENT: {
		code: 204,
		message: 'No Content'
	},
	SEE_OTHER: {
		code: 303,
		message: 'See Other'
	},
	NOT_MODIFIED: {
		code: 304,
		message: 'Not Modified'
	},
	// Request Errors: 4.x
	BAD_REQUEST: {
		code: 400,
		message: 'Bad Request'
	},
	UNAUTHORIZED: {
		code: 401,
		message: 'Unauthorized'
	},
	FORBIDDEN: {
		code: 403,
		message: 'Forbidden'
	},
	NOT_FOUND: {
		code: 404,
		message: 'Not Found'
	},
	CONFLICT: {
		code: 409,
		message: 'Conflict'
	},
	UNPROCESSABLE_ENTITY: {
		code: 422,
		message: 'Unprocessable Entity'
	},
	// Server errors: 5.x
	INTERNAL_SERVER_ERROR: {
		code: 500,
		message: 'Internal Server Error'
	}
}

