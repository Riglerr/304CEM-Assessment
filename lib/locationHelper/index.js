'use strict'

const co = require('co')
const PlacesApi = require('places-api')
const InvalidArgumentError = require('custom-errors/InvalidArgumentError')
const LocationNotFound = require('custom-errors/notFound/LocationNotFoundError')
const InvalidLocationError = require('custom-errors/location/InvalidLocationError')

/**
 *
 * @description Contains a set of utility functions for the location collection
 * @class LocationHelper
 */
class LocationHelper {
	/**
	 * Creates an instance of LocationHelper.
	 *
	 * @param {string} apiKey - Google places API Key
	 *
	 * @memberOf LocationHelper
	 */
	constructor(apiKey) {
		this.api = new PlacesApi(apiKey)

		//bindings
		this.getByLatLong = this.getByLatLong.bind(this)
		this.getById = this.getById.bind(this)
	}

	/**
	 * @name getLocationsByLatLong
	 * @description Attempts to retrieve a collection of locations
	 * 	within a 10 mile radius of the given latitude and longitude.
	 * @memberof LocationHelper
	 * @param {string} latitude - The latitude
	 * @param {string} longitude - The longitude
	 * @param {string} keyword - The keyword
	 * @returns {Promise<Array>} - A collection of locations.
	 *
	 * @memberof LocationHelper
	 */
	getByLatLong(latitude, longitude, keyword) {
		if (!latitude || typeof latitude !== 'string') {
			throw new InvalidArgumentError('latitude', 'latitude was null or not a string')
		}
		if (!longitude || typeof longitude !== 'string') {
			throw new InvalidArgumentError('longitude', 'longitude was null or not a string')
		}
		const { api } = this

		return co(function* () {
			try {
				let places

				if (keyword) {
					places = yield api.nearbySearch(latitude, longitude, '10000', keyword)
				} else {
					places = yield api.nearbySearch(latitude, longitude, '10000')
				}

				return places
			} catch(err) {
				if (err.status === 'INVALID_REQUEST') {
					throw new InvalidLocationError('There was an issue with your query, check that all values are correct.')
				} else {
					throw err
				}
			}
		})
	}

	/**
	 * @name getById
	 * @description - Attempts to find a specific location by its id.
	 * @memberof LocationHelper
	 * @param {string} id - A location Id
	 * @returns {Promise<Object>} - A location object.
	 *
	 * @memberof LocationHelper
	 */
	getById(id) {
		if (!id || typeof id !== 'string') {
			throw new InvalidArgumentError('id', 'id was null or not a string')
		}
		const { api } = this


		return co(function* () {
			let place

			try {
				place = yield api.details(id)
			} catch(err) {
				if (err.status === 'INVALID_REQUEST') {
					throw new LocationNotFound(id)
				}
			}

			return place
		})
	}

}

module.exports = LocationHelper
