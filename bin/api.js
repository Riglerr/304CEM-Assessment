'use strict'

const initServer = require('../config/init/server')
const initDb = require('../config/init/db')

initDb()
	.then(() => {
		initServer()
	})
	.catch((err) => {
		console.log(err)
	})
