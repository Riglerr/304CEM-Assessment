'use strict'

const host = require('../../config').host
const { Event, Location } = require('../models')
const getIdFromRequestParams = require('../../lib/request/getIdFromRequest')
const validateEventPayload = require('../../lib/request/validateEventPayload')
const { OK, CREATED, NO_CONTENT, NOT_MODIFIED } = require('status-codes')

const InvalidLocationError = require('custom-errors/location/InvalidLocationError')
const InvalidEventError = require('custom-errors/event/InvalidEventError')
const LocationNotFoundError = require('custom-errors/notFound/LocationNotFoundError')
const ForbiddenError = require('custom-errors/auth/ForbiddenError')
const EventSerializer = require('api-serializers/event')
const serializer = new EventSerializer(host)

/**
 * @description A Route controller that returns a collection of event documents
 *
 * @param {any} ctx - Koa context
 * @returns {undefined} -
 */
module.exports.getEvents = function* (ctx) {
	let events
	const { search, sort, limit} = ctx.query

	if (search || sort || limit) {
		events = yield Event.findWithQuery(search, sort, limit)
	} else {
		events = yield Event.find({})
	}

	events = yield Promise.all(events.map((event) => event.populate('location').execPopulate()))
	const response = serializer.serializeMultiple(events)

	ctx.body = response
	ctx.status = OK.code
}

/**
 * @description Gets a specific document from the event mongo collection
 *
 * @param {any} ctx - Koa context
 * @returns {undefined} -
 */
module.exports.getEventById = function *(ctx) {
	const id = getIdFromRequestParams(ctx.params)
	const lastModified = new Date(ctx.headers['if-modified-since'])
	const event = yield Event.findByEventId(id)

	if (lastModified && event.lastModified < lastModified) {
		ctx.status = NOT_MODIFIED.code
	} else {
		yield event.populate('location').execPopulate()
		const response = serializer.serializeSingle(event)

		ctx.set('last-modified', event.lastModified)
		ctx.body = response
		ctx.status = OK.code
	}
}

/**
 * @name leaveEvent
 * @description Route Controller for un-attending an event
 * @param {any} ctx - Koa Request context
 * @returns {undefined} -
 */
module.exports.leaveEvent = function *(ctx) {
	const id = getIdFromRequestParams(ctx.params)
	const event = yield Event.findByEventId(id)
	const indexOfUser = event.attending.indexOf(ctx.state.user.username)

	if (indexOfUser > -1) {
		event.attending.splice(indexOfUser, 1)
		yield event.save()
	}

	ctx.status = NO_CONTENT.code
}

/**
 * @name joinEvent
 * @description Route Controller for attending an event
 * @param {any} ctx - Koa Request context
 * @returns {undefined} -
 */
module.exports.joinEvent = function* (ctx) {
	const id = getIdFromRequestParams(ctx.params)
	const event = yield Event.findByEventId(id)
	const isAttending = event.attending.indexOf(ctx.state.user.username) > -1

	if(!isAttending) {
		event.attending.push(ctx.state.user.username)
		yield event.save()
	}
	ctx.status = NO_CONTENT.code
}

/**
 * @name getEventLocation
 * @description Gets the location details for the specific event
 * @param {any} ctx - Koa request context
 * @returns {undefined} -
 */
module.exports.getEventLocation = function*(ctx) {
	const id = getIdFromRequestParams(ctx.params)
	const event = yield Event.findByEventId(id)

	yield event.populate('location').execPopulate()
	const response = event.location.serialize(event)

	ctx.body = response
	ctx.status = OK.code
}

/**
 * @name createEvent
 * @description Route Controller for creating an event
 *
 * @param {any} ctx - Koa context
 * @returns {undefined} -
 */
module.exports.createEvent = function *(ctx) {
	const payload = yield validateEventPayload(ctx.request)

	if(!payload) throw new InvalidEventError('The request payload was improperly formatted')
	const eventLocation = yield Location.ensureLocationExists(payload.location)

	if (!eventLocation) {
		throw new InvalidLocationError('The location specified does not exist.')
	}
	const event = new Event(payload)

	event.creator = ctx.state.user.username
	yield event.save()
	yield event.populate('location').execPopulate()

	ctx.set('location', `https://${host}/events/${event._id}` )
	ctx.status = CREATED.code
	ctx.body = serializer.serializeSingle(event)
}

/**
 * @name updateEvent
 * @description Route handler for updating an event
 * @param {object} ctx - Koa request context
 * @returns {undefined} -
 */
module.exports.updateEvent = function* (ctx) {
	const id = getIdFromRequestParams(ctx.params)
	const event = yield Event.findByEventId(id)

	if (event.creator !== ctx.state.user.username) {
		throw new ForbiddenError('You must be the owner of this resource to update it.')
	}
	const payload = yield validateEventPayload(ctx.request, true, id)

	if(!payload) throw new InvalidEventError('The request payload was improperly formatted')
	const eventLocation = yield Location.ensureLocationExists(payload.location)

	if (!eventLocation) {
		throw new LocationNotFoundError(payload.location)
	}
	const updatedEvent = yield event.patch(payload)

	yield updatedEvent.populate('location').execPopulate()

	ctx.status = OK.code
	ctx.set('location', `https://${host}/events/${event._id}` )
	ctx.body = serializer.serializeSingle(updatedEvent)
}

/**
 * @name deleteEvent
 * @description Route controller for deleting an event
 * @param {object} ctx - Koa request context
 * @returns {undefined} -
 */
module.exports.deleteEvent = function* (ctx) {
	const id = getIdFromRequestParams(ctx.params)
	const event = yield Event.findByEventId(id)

	if (event.creator !== ctx.state.user.username) {
		throw new ForbiddenError('You must be the owner of this resource to update it.')
	}
	yield event.remove()

	ctx.status = NO_CONTENT.code
}
