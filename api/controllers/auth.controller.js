'use strict'

const path = require('path')
const User = require('../models/user.model.js')
const config = require('../../config')

// StatusCodes
const { OK, CREATED } = require('status-codes')

// Auth Utilities
const libPath = '../../lib/'
const authLibPath = path.join(libPath, 'auth/')
const getCredentialsFromRequest = require(path.join(libPath, 'request/', 'getCredentialsFromRequest'))
const validatePassword = require(path.join(authLibPath, 'validatePassword'))
const signJWT = require(path.join(authLibPath, 'signJWT'))
const validateCredentials = require(path.join( authLibPath,'validateCredentials'))


// Error
const UsernameConflictError = require('custom-errors/user/UsernameConflictError')

/**
 *
 *
 * @class AuthController
 */
class AuthController {

	/**
	 * @description Authenticates a user and returns a JsonWebToken for api access.
	 * @memberof AuthController
	 * @param {any} ctx - Koa Request Context
	 * @returns {null|undefined} -
	 *
	 * @throws {InvalidCredentialsError} - If Request Payload is invalid.
	 * @throws {UserNotFoundError} - If the intended user does not exist.
	 * @throws {IncorrectPasswordError} - If the supplied password does not match the stored password.
	 */
	static *login(ctx) {
		const credentials = getCredentialsFromRequest(ctx.request)
		const {username, password} = validateCredentials(credentials)
		const user = yield User.findByUsername(username)

		yield validatePassword(user, password)
		const token = yield signJWT(user)

		ctx.body = {
			user: user.username,
			token
		}
		ctx.status = OK.code
	}

	/**
	 * @description Registers a new user for use throughout the system.
	 * @memberof AuthController
	 * @param {Object} ctx - Koa Request Context
	 * @returns {null|undefined} -
	 *
	 * @throws {InvalidCredentialsError} - Invalid Payload
	 * @throws {UsernameConflictError} - If the username prodided is already registered.
	 */
	static *register(ctx) {
		let eusername

		try {
			const credentials = getCredentialsFromRequest(ctx.request)
			const {username, password} = validateCredentials(credentials)

			eusername = username
			const newUser = new User({username, password})

			yield newUser.save()
			ctx.status = CREATED.code
			ctx.body = {
				meta: {
					login: {
						method: 'POST',
						href: `https://${config.host}/auth/login`
					}
				}
			}
		} catch(err) {
			const duplicateErrorCode = 11000

			if (err && err.code === duplicateErrorCode) {
				throw new UsernameConflictError(eusername)
			}
			throw err
		}
	}
}

module.exports = AuthController
