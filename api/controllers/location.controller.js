'use strict'

const validateLocationQuery = require('../../lib/request/validateLocationQuery')
const getIdFromRequest = require('../../lib/request/getIdFromRequest')
const LocationHelper = require('location-helper')
const config = require('../../config')
const { OK } = require('status-codes')
const Location = require('../models/location.model.js')

/**
 * Controlls the Locations endpoint
 *
 * @class LocationController
 */
class LocationController {
	/**
	 * Retrieves locations from the Google Places API
	 *
	 * @static
	 * @param {Object} ctx - Koa request context
	 *
	 * @memberof LocationController
	 * @returns {undefined} -
	 */
	static *discover(ctx) {
		const query = validateLocationQuery(ctx.request)
		const locations = new LocationHelper(config.placesApiKey)
		const places = yield locations.getByLatLong(query.latitude, query.longitude, query.keyword)
		const response = Location.serialize(places)

		ctx.status = OK.code
		ctx.body = response
	}

	/**
	 *
	 * @description - Route Controller for retrieving a single location by it's id
	 * @static
	 * @param {any} ctx - Koa request context
	 *
	 * @memberof LocationController
	 * @returns {undefined} -
	 */
	static *getById(ctx) {
		const id = getIdFromRequest(ctx.params)
		const locations = new LocationHelper(config.placesApiKey)
		const place = yield locations.getById(id)
		const response = Location.serialize(place)

		ctx.status = OK.code
		ctx.body = response
	}
}

module.exports = LocationController
