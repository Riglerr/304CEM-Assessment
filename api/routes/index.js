'use strict'

const Router = require('koa-router')
const co = require('co')

const events = require('./event.routes.js')
const auth = require('./auth.routes.js')
const locations = require('./location.routes.js')
const modules = [
	events,
	locations,
	auth
]

/**
 * @description Binds Api Routes to a Koa Server instance.
 * @param {object} server - Koa server instance
 * @returns {undefined} - Undefined
 */
module.exports = function(server) {
	modules.forEach(({baseUrl, routes}) => {
		const routerInstance = new Router({prefix: baseUrl})

		routes.forEach(({method, route, handlers}) => {

			// Parse Route
			const routeMethod = method.toLowerCase()

			// Enable Generators for handlers
			const newHandlers = handlers.map((item) => co.wrap(item))
			// Map handlers to the route
			const lastHandle = newHandlers.pop()

			routerInstance[routeMethod](route, ...newHandlers, co.wrap(function *(ctx) {
				return yield lastHandle(ctx)
			}))
		})
		server
			.use(routerInstance.routes())
			.use(routerInstance.allowedMethods())
	})
}
