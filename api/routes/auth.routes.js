'use strict'

// =============================
//           API DOC
// =============================

/**
 * @api {post} /auth/login Request User information
 * @apiName Login
 * @apiGroup Auth
 *
 * @apiParam {string} username The users unqiue username.
 * @apiParam {string} password The users password.
 *
 * @apiSuccess (200) {user} The users unqiue username.
 * @apiSuccess (200) {token} The access token used to access the restricted api.
 */

/**
 * @api {post} /auth/register Register a new user with the service.
 * @apiName Register
 * @apiGroup Auth
 *
 * @apiParam {string} username The users unqiue username.
 * @apiParam {string} password The users password.
 *
 * @apiSuccess (201) {object} links A group of related links.
 * @apiSuccess (201) {string} links.login A relative link to the login endpoint.
 */

const { login, register } = require('../controllers/auth.controller.js')

module.exports = {
	baseUrl: '/auth',
	routes: [
		{
			method: 'POST',
			route: '/login',
			handlers: [
				login
			]
		},
		{
			method: 'POST',
			route: '/register',
			handlers: [
				register
			]
		}
	]
}

