'use strict'

/**
 * @api {get} /locations - Discover locations.
 * @apiName discover
 * @apiGroup Locations
 *
 * @apiParam {latitude} latitude value
 * @apiParam {longitude} longitude value
 * @apiParam {keyword} keyword filter value
 *
 *
 * @apiError (400) INVALID_REQUEST Invalid query parameters provided.
 */

/**
 * @api {get} /locations/:id - Retrieve a single location by it's placeid
 * @apiName getById
 * @apiGroup Locations
 *
 * @apiParam {id} - The intended locations placeid
 *
 */

const LocationController = require('../controllers/location.controller.js')

module.exports = {
	baseUrl: '/locations',
	routes: [
		{
			method: 'GET',
			route: '/',
			handlers: [
				LocationController.discover
			]
		},
		{
			method: 'GET',
			route: '/:id',
			handlers: [
				LocationController.getById
			]
		}
	]
}
