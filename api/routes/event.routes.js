'use strict'

// Controllers
const {
	getEvents,
	getEventById,
	createEvent,
	updateEvent,
	deleteEvent,
	getEventLocation,
	joinEvent,
	leaveEvent
} = require('../controllers/event.controller.js')

// Middlewear
const authenticate = require('../middlewear/authenticate')

/**
 * @api {get} /events/ Request collection of all events
 * @apiName GetEvents
 * @apiGroup event
 *
 * @apiSuccess {string} Balls
 */

/**
 * @api {get} /events/:id Request a specific event by its Id.
 * @apiName getEventById
 * @apigroup event
 *
 * @apiSuccess {string} Balls
 */

/**
 * @api {get}
 */
module.exports = {
	baseUrl: '/events',
	routes: [
		{
			method: 'GET',
			route: '/',
			handlers: [
				getEvents
			]
		},
		{
			method: 'GET',
			route: '/:id',
			handlers: [
				getEventById
			]
		},
		{
			method: 'GET',
			route: '/:id/relationships/location',
			handlers: [
				getEventLocation
			]
		},
		{
			method: 'GET',
			route: '/:id/join',
			handlers: [
				authenticate,
				joinEvent
			]
		},
		{
			method: 'GET',
			route: '/:id/leave',
			handlers: [
				authenticate,
				leaveEvent
			]
		},
		{
			method: 'POST',
			route: '/',
			handlers: [
				authenticate,
				createEvent
			]
		},
		{
			method: 'PATCH',
			route: '/:id',
			handlers: [
				authenticate,
				updateEvent
			]
		},
		{
			method: 'DELETE',
			route: '/:id',
			handlers: [
				authenticate,
				deleteEvent
			]
		}
	]
}
