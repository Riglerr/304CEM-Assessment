'use strict'
const co = require('co')
const { INTERNAL_SERVER_ERROR, UNAUTHORIZED } = require('status-codes')

const { InvalidArgumentError, ExtensibleError } = require('custom-errors')
const JsonWebTokenError = require('jsonwebtoken/lib/JsonWebTokenError')

/**
 * @name errorHandler
 * @description Catches errors thrown by routes and creates an api response.
 *
 * @returns {null|undefined} -
 */
module.exports = function() {
	return co.wrap(function *(ctx, next) {
		try {
			yield next()
		} catch (error) {
			const env = process.env.NODE_ENV || 'development'

			if (error instanceof JsonWebTokenError) {
				ctx.status = UNAUTHORIZED.code
				ctx.body = {
					errors: [
						{
							error: error.name,
							message: error.message,
						}
					]
				}
				if (error.expiredAt) {
					ctx.body.errors[0].expiredAt = error.expiredAt
				}
			} else if (error instanceof ExtensibleError && !(error instanceof InvalidArgumentError)) {
				ctx.status = error.statusCode || INTERNAL_SERVER_ERROR.code
				ctx.body = {
					errors: [
						{
							error: error.name,
							message: error.message
						}
					]
				}
			} else if(env === 'production') {
				//Hide Error in production
				ctx.status = INTERNAL_SERVER_ERROR.code
				ctx.body = {errors: { error: 'An Internal Error occurred, please contact a system administrator.'}}
			} else {
				ctx.status = INTERNAL_SERVER_ERROR.code
				ctx.body = {errors: { error: error.message || error}}
				ctx.stacktrace = error.stacktrace || null
			}
		}
	})
}
