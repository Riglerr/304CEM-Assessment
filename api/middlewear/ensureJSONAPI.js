'use strict'

const co = require('co')
const BadRequest = require('custom-errors/BadRequest')

/**
 * @name ensureJSONAPIHeader
 * @description Ensures that the Content-Type header is set to application/vnd.api+json where required
 * @param {any} ctx
 * @param {any} next
 * @returns
 */

module.exports = function() {
	return co.wrap(function *(ctx, next) {
		// All routes other than /auth/ require content-type: application/vnd.api+json
		if(ctx.path.indexOf('/auth/') === -1 && ctx.path !== '/') {
			const contentType = ctx.headers['content-type']
			const error = new BadRequest('This route requires a Content-Type header of \'application/vnd.api+json\'')

			if (!contentType) throw error
			if(contentType.toLowerCase() !== 'application/vnd.api+json') throw error
		}
		yield next()
		ctx.set('content-type', 'application/vnd.api+json')
	})
}

