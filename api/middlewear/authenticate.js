'use strict'

const getAuthHeaderFromRequest = require('../../lib/request/getAuthorizationHeaderFromRequest')
const decodeAuthorizationHeader = require('../../lib/auth/decodeAuthorizationHeader')
const User = require('../models/user.model.js')


// Errors
const UnauthorizedError = require('custom-errors/auth/UnauthorizedError')

/**
 * @name authenticate
 * @description Koa middlewear used to ensure that authenticate an incoming request.
 * @param {Object} ctx - The Request context
 * @param {function} next - The next function.
 * @returns {undefined} -
 */
module.exports = function *(ctx, next) {
	const authorizationHeader = getAuthHeaderFromRequest(ctx)

	if (!authorizationHeader) {
		ctx.set('www-authenticate', 'Bearer')
		throw new UnauthorizedError()
	}

	const tokenData = yield decodeAuthorizationHeader(authorizationHeader)
	const user = yield User.findByUsername(tokenData.user)

	ctx.state.user = user
	yield next()
}
