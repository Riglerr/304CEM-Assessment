'use strict'

const mongoose = require('mongoose')
const connection = require('../../config/getDbConnection')(mongoose)
const co = require('co')
const JSONAPISerializer = require('jsonapi-serializer')

const config = require('../../config')
const placesApiKey = config.placesApiKey
const collectionUrl = `https://${config.host}/locations/`
const eventCollectionUrl = `https://${config.host}/events/`

// Errors
const InvalidArgumentError = require('custom-errors/InvalidArgumentError')
const LocationHelper = require('location-helper')

const serializerOpts = {
	id: '_id',
	attributes: ['name', 'dateAdded', 'latitude', 'longitude', 'types'],
	keyForAttribute: 'camelCase'
}

const LocationSchema = new mongoose.Schema({
	_id: { type: String, required: true },
	dateAdded: { type: Date },
	name: { type: String },
	latitude: { type: Number },
	longitude: { type: Number }
})

/**
 * @name ensureLocationExists
 * @description Ensures that a location exists either in Mongo or on the place API
 * @param {string} placeId - The id of the location to find.
 * @return {Promise<Object>} -
 */
LocationSchema.statics.ensureLocationExists = function(placeId) {
	const Location = this

	return co(function* () {
		if (!placeId) throw new InvalidArgumentError('placeId', 'Argument placeId cannot be null')
		let place = yield Location.findById(placeId)

		if (place) {
			return place
		}
		try {
			const LocationsApi = new LocationHelper(placesApiKey)

			place = yield LocationsApi.getById(placeId)
			place = LocationSchema.statics.serialize(place)
			const newPlace = new Location({
				_id: place.data.id,
				name: place.data.attributes.name,
				dateAdded: new Date(),
				latitude: place.data.attributes.geometry.location.lat,
				longitude: place.data.attributes.geometry.location.lng
			})

			yield newPlace.save()
			return newPlace
		} catch(err) {
			return null
		}
	})
}

LocationSchema.methods.serialize = function(event) {
	let links

	 if (event) {
		links ={
			topLevelLinks: {
				self: () => `${eventCollectionUrl}${event._id}/relationships/location`,
				related: () => `${eventCollectionUrl}${event._id}`
			},
			dataLinks: {
				self: (dataSet, location) => `${collectionUrl}${location._id}`
			}
		}
	} else {
		links = {
			topLevelLinks: {
				self: (location) => `${collectionUrl}${location.id}`
			}
		}
	}
	return new JSONAPISerializer.Serializer('locations',
		Object.assign({}, serializerOpts, links)
	).serialize(this)
}

LocationSchema.statics.serialize = function(location) {
	return new JSONAPISerializer.Serializer('locations', {
		id: 'place_id',
		attributes: ['geometry', 'icon', 'name', 'types'],
		geometry: {
			attributes: ['location'],
			location: {
				attributes: ['lat', 'lng']
			}
		},
		topLevelLinks: {
			self: () => `${collectionUrl}`
		},
		dataLinks: {
			self: (dataSet, location) => `${collectionUrl}${location.place_id}`
		}
	}).serialize(location)
}


module.exports = connection.model('Location', LocationSchema)
