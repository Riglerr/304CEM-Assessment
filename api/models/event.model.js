'use strict'
const mongoose = require('mongoose')
const co = require('co')
const connection = require('../../config/getDbConnection')(mongoose)
const Location = require('./location.model.js')
const defaultLimit = 100
const EventNotFoundError = require('custom-errors/notFound/EventNotFoundError')
const BadRequest = require('custom-errors/BadRequest')

const EventSchema = new mongoose.Schema({
	title: {type: String, required: true},
	description: {type: String, required: true},
	startDate: {type: Date, required: true},
	endDate: {type: Date, required: true},
	creator: {type: String, required: true},
	attending: { type: [String] },
	keywords: { type: [String] },
	location: {type: String, ref: 'Location', required: true},
	lastModified: {type: Date}
})

EventSchema.index({'$**': 'text'})

EventSchema.pre('save',function(next) {
	const event = this

	co(function* () {
		if (typeof event.location === 'string') {
			yield Location.ensureLocationExists(event.location)
		}
		event.lastModified = new Date(Date.now())
		next()
	})
})

EventSchema.methods.patch = function(newData) {
	let event = this

	return co(function *() {
		event = Object.assign(event, newData)
		return yield event.save()
	})
}

EventSchema.statics.findByEventId = function(id) {
	const Event = this

	if (!id) throw new EventNotFoundError('null')
	return co(function* () {
		try {
			const event = yield Event.findById(id)

			if (!event) throw new EventNotFoundError(id)
			return event
		} catch(err) {
			throw new EventNotFoundError(id)
		}
	})
}

/**
 * @name findWithQuery
 * @description - Returns a collection of events based on several query parameters.
 * @param {any} searchString - Text search for int document
 * @param {any} sortBy - Field to sort by e.g. '-title' to sort by descending title.
 * @param {any} limit - Maximum number of docuemnts to return (default 100)
 * @returns {Promise<Array>} - Array of event documents
 */
EventSchema.statics.findWithQuery = function(searchString, sortBy, limit) {
	const Event = this

	return co(function* () {
		if (!searchString) {
			limit = limit ? parseInt(Math.abs(limit)) : defaultLimit
			if (!limit) throw new BadRequest('limit query value must be an integer')
			return yield Event.find({})
				.sort(sortBy || null)
				.limit(limit)
				.exec()
		}
		return yield Event.find({$text: {$search: searchString}})
			.sort(sortBy || null)
			.limit(limit || defaultLimit)
			.exec()
	})
}


module.exports = connection.model('Event', EventSchema)
