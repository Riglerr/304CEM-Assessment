'use strict'

const mongoose = require('mongoose')
const bcrypt = require('bcrypt')
const saltRounds = require('../../config').saltRounds
const connection = require('../../config/getDbConnection')(mongoose)

// Custom Error Codes
const UserNotFoundError = require('custom-errors/notFound/UserNotFoundError')
const InvalidArgumentError = require('custom-errors/InvalidArgumentError')
const InvalidCredentialsError = require('custom-errors/auth/InvalidCredentialsError')

// Co - Promise-Generator library
const co = require('co')

const UserSchema = new mongoose.Schema({
	username: {type: String, required: true, unique: true},
	password: {type: String, required: true},
	createdDate: {type: Date, require: true}
})

/**
 * @name findByUsername
 * @description Attempt to find a User document by the username field.
 * @param {string} username - The username to search for.
 * @returns {User} The user document.
 * @throws {InvalidArgumentError} - Thrown if username is null or not a string.
 * @throws {UserNotFoundError} - Thrown if a user with that username does not exist.
 */
UserSchema.statics.findByUsername = co.wrap(function *(username) {
	if (!username || typeof username !== 'string') {
		throw new InvalidArgumentError('username', 'The username field was empty or of an invalid type.')
	}
	const user = yield this.findOne({username})

	if (!user) {
		throw new UserNotFoundError(username)
	}
	return user
})

/**
 * @name matchPasswordWithHash
 * @description Verifies that a password is correct.
 * @param {string} password - The password attempt.
 * @returns {Promise<boolean>} - True if match, false if not.
 */
UserSchema.methods.matchPasswordWithHash = function(password) {
	const actualUserPassword = this.password

	return co(function* () {
		return password ? yield bcrypt.compare(password, actualUserPassword) : false
	})
}

/**
 * @name User.preSave
 * @description Responsible for validating a user object before it is created or updated.
 *
 */
UserSchema.pre('save', function(next) {
	const user = this

	if (!user.isModified('password')) {
		return next()
	}
	return co(function* () {
		user.password = yield UserSchema.statics.hashPassword(user.password)
		next()
	})
})

/**
 * @description - Hashes a string according to the configured format.
 *
 * @param {string} password - The password to hash
 * @returns {Promise<String>} - string hash
 * @throws {InvalidCredentialsError}
 */
UserSchema.statics.hashPassword = function hashPassword(password) {
	return co(function* () {
		if(!password || typeof password !== 'string') {
			throw new InvalidCredentialsError('password was empty or invalid')
		}
		const hash = yield bcrypt.hash(password, saltRounds)

		return hash
	})
}

module.exports = connection.model('User', UserSchema)
